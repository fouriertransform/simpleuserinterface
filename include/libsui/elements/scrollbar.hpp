/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_SCROLLBAR_HPP
#define LIBSUI_ELEMENTS_SCROLLBAR_HPP

#pragma once

#include <functional> // std::function
#include <cmath> // std::ceil

#include "base_element.hpp"

namespace libsui {
	/**
	 * @brief This class represents a scrollbar element.
	 */
	class Scrollbar : public BaseElement {
	private:
		/**
		 * @brief Indicates the minimum height of the thumb button.
		 */
		constexpr static std::int32_t kMinimumThumbHeight = 45;

		/**
		 * @brief The index to be displayed at the top of the list.
		 * @note Represents the current tick.
		 */
		int* m_first_index = nullptr;

		/**
		 * @brief Total amount of scroll ticks.
		 */
		int m_total_ticks = 0;

		/**
		 * @brief Height of the thumb button.
		 */
		int m_thumb_height = 0;

		/**
		 * @brief Vertical thumb button offset, relative to current position.
		 */
		int m_thumb_offset = 0;

	public:
		/**
		 * @brief Initializes this @ref Scrollbar instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] size The clickable size of the current element.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Scrollbar(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size
		)
			: BaseElement( parent, position, size, "", kType_Scrollbar ) {}

		/**
		 * @brief Initializes this scrollbar instance.
		 * @param [in] tick The current scrollbar tick.
		 * @param [in] total_ticks The total number of scrollbar ticks.
		 * @param [in,out] first_index The first index in the entry list to be shown.
		 */
		void Initialize( int tick, int total_ticks, int* first_index ) {
			m_total_ticks = total_ticks;
			m_first_index = first_index;
			m_thumb_height = std::max( m_size.m_y / ( std::max( 1, total_ticks ) ), kMinimumThumbHeight );

			SetTick( tick );
		}

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 */
		bool OnPress() override {
			if( m_total_ticks <= 1 )
				return false;

			auto ticks_per_pixel = static_cast< float >( m_total_ticks );

			const auto surface = m_size.m_y - m_thumb_height;
			if( surface != 0 )
				ticks_per_pixel /= surface;

			const auto center = ( GetAbsolutePosition().m_y + m_thumb_height / 2 );
			const auto move_offset = std::max( g_mouse->m_position.m_y - center, 0 );
			const auto next_tick = std::lround( move_offset * ticks_per_pixel );

			SetTick( static_cast< int >( next_tick ) );
			return true;
		}

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 */
		void OnRelease() override {}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 */
		void Render() override {
			const auto position = GetAbsolutePosition();

			// Draw background box
			g_renderer->Fill( position, m_size, argb( 40, 40, 40 ) );

			// There is nothing to scroll, so don't draw the thumb
			if( !m_total_ticks )
				return;

			// Draw scrollbar thumb
			g_renderer->FillOutlinedRect(
				position.m_x,
				position.m_y + m_thumb_offset,
				m_size.m_x,
				m_thumb_height,
				argb( 65, 65, 65 ),
				( Hovered() || m_state & kState_Focused ) ? argb( 180, 150, 115 ) : argb( 40, 40, 40 )
			);
		}

		/**
		 * @brief Updates @p m_thumb_offset based on the current tick
		 * @param [in] tick The tick that should be set.
		 */
		void SetTick( int tick ) {
			// Calculate the next tick
			const auto next_tick = std::clamp( tick, 0, m_total_ticks );

			// Calculate the relative offset amount
			auto pixels_per_tick = static_cast< float >( m_size.m_y - m_thumb_height );
			if( m_total_ticks > 0 )
				pixels_per_tick /= m_total_ticks;

			// Set new first index
			*m_first_index = next_tick;

			// Calculate thumb button offset
			m_thumb_offset = static_cast< int >( next_tick * pixels_per_tick );
		}

		/**
		 * @brief Gets the total amount of scrollable ticks.
		 * @return The total amount of scroll ticks.
		 */
		[[nodiscard]] inline int GetTotalTicks() const {
			return m_total_ticks;
		}
	};
}

#endif // !LIBSUI_ELEMENTS_SCROLLBAR_HPP
