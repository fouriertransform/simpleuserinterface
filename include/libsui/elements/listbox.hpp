/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_LISTBOX_HPP
#define LIBSUI_ELEMENTS_LISTBOX_HPP

#pragma once

#include <utility>
#include <vector>

#include "base_element.hpp"
#include "scrollbar.hpp"

#include <cmath>

namespace libsui {
	/**
	 * @brief This class represents a single-choice listbox element.
	 * If there are more than @ref kEntryLimit elements in the listbox, then a scrollbar is shown.
	 */
	class Listbox : public BaseElement {
	public:
		/**
		 * @brief Constant which determines how many entries should be displayed at once.
		 */
		constexpr static auto kEntryLimit = 12;

		/**
		 * @brief Constant which determines the height of an entry.
		 */
		constexpr static auto kEntryHeight = 15;

		/**
		 * @brief Constant which determines the width of the underlying scrollbar element.
		 */
		constexpr static auto kScrollbarWidth = 10;

	private:
		/**
		 * @brief Internal type used to represent a listbox entry.
		 */
		using Entry = std::pair< int, std::string >;

		/**
		 * @brief Internal type used to hold all listbox entries.
		 */
		using EntryContainer = std::vector< Entry >;

		/**
		 * @brief Variable to be used for interacting with this element.
		 */
		int& m_variable;

		/**
		 * @brief Currently selected entry.
		 * @note nullptr if no entry is selected.
		 */
		Entry* m_selected_entry = nullptr;

		/**
		 * @brief Container to hold all entries.
		 */
		EntryContainer m_entries;

		/**
		 * @brief Index in the @ref m_entries container of the item
		 * selected on construction.
		 */
		int m_first_index = 0;

		/**
		 * @brief The scrollbar subelement.
		 */
		Scrollbar m_scrollbar;

	private:
		/**
		 * @brief Computes the number of ticks a user can scroll with the scrollbar.
		 * @return Number of ticks a user can scroll with the scrollbar.
		 */
		[[nodiscard]] inline auto GetScrollbarTicks() const -> int {
			return std::max( 0, static_cast< int >( m_entries.size() ) - kEntryLimit );
		}

		/**
		 * @brief Acquires the position of the underlying @p m_scrollbar element relative to the @ref m_anchor member.
		 * @return Position of the underlying @p m_scrollbar element relative to the @ref m_anchor member.
		 */
		[[nodiscard]] constexpr inline Vector2D GetScrollbarPosition() const {
			return { m_size.m_x - kScrollbarWidth, 0 };
		}

		/**
		 * @brief Acquires the size of the underlying @p m_scrollbar element.
		 * @return The size of the underlying @p m_scrollbar element, in pixels.
		 */
		[[nodiscard]] constexpr inline Vector2D GetScrollbarSize() const {
			return { kScrollbarWidth, m_size.m_y + 2 };
		}

		/**
		 * @brief Determines whether the current entry is hovered.
		 * @param [in] index Index of the current element in the @ref m_entries container.
		 * @note This function is only invoked when the dropdown menu is open.
		 * @return true, if mouse is hovering the current element
		 * @return false, if mouse is not hovering the current element
		 */
		[[nodiscard]] bool HoveredEntry( const std::int32_t index ) const {
			if( !CanFocus() )
				return false;

			if( m_scrollbar.m_state & kState_Focused )
				return false;

			const auto position = GetAbsolutePosition();
			return g_mouse->Over(
				position.m_x,
				position.m_y + index * kEntryHeight,
				m_size.m_x - kScrollbarWidth,
				kEntryHeight
			);
		}

		/**
		 * @brief Helper function used in the constructor to avoid repetitive code.
		 * @param [in,out] variable The variable which should be modified on value change.
		 * @param [in] list_size The size of the entry list.
		 */
		void Initialize() {
			const auto list_size = static_cast< int >( m_entries.size() );
			if( m_variable < 0 || m_variable >= list_size )
				m_variable = 0;

			if( list_size > 0 ) {
				m_selected_entry = &m_entries[ m_variable ];
				m_first_index = m_selected_entry->first;
			}

			const auto tick = m_first_index > kEntryLimit ? m_first_index : 0;
			m_scrollbar.Initialize( tick, GetScrollbarTicks(), &m_first_index );
			m_scrollbar.m_anchor = m_anchor + GetScrollbarPosition();
		}

	public:
		/**
		 * @brief Initializes this @ref Listbox instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] size The clickable size of the current element.
		 * @param [in,out] variable The variable which should be modified on value change.
		 * @param [in] entries List of entries that should populate this element.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Listbox(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size,
			int& variable,
			EntryContainer entries = {}
		)
			: BaseElement( parent, position, size, "", kType_Listbox )
			, m_variable( variable )
			, m_entries( std::move( entries ) )
			, m_first_index( 0 )
			, m_scrollbar( this, position, GetScrollbarSize() ) {
			Initialize();
		}

		/**
		 * @brief Initializes this @ref Listbox instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] size The clickable size of the current element.
		 * @param [in,out] variable The variable which should be modified on value change.
		 * @param [in] entries List of entries that should populate this element.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Listbox(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size,
			int& variable,
			const std::vector< std::string >& entries
		)
			: BaseElement( parent, position, size, "", kType_Listbox )
			, m_variable( variable )
			, m_first_index( 0 )
			, m_scrollbar( this, position, GetScrollbarSize() ) {
			Update( entries );
		}

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 * @note This function renders the underlying @ref Scrollbar element.
		 */
		bool OnPress() override {
			switch( g_mouse->m_scroll_direction ) {
			case kScroll_Down:
			case kScroll_Up:
				// We can't scroll, ergo don't consume input.
				if( !m_scrollbar.GetTotalTicks() )
					return false;

				// Update scroll position according to the scroll amount.
				m_scrollbar.SetTick( m_first_index + g_mouse->m_scroll_amount );
				return true;
			default: break;
			}

			// Handle scrollbar events, if required
			if( m_scrollbar.Hovered() || ( m_scrollbar.m_state & kState_Focused ) ) {
				m_scrollbar.m_state |= kState_Focused;
				return m_scrollbar.OnPress();
			}

			// What is the index of the last listbox entry we can draw?
			const auto display_limit = std::min(
				m_first_index + kEntryLimit,
				static_cast< int >( m_entries.size() )
			);

			// Iterate over drawn listbox entries
			for( auto i = m_first_index; i < display_limit; ++i ) {
				if( !HoveredEntry( i - m_first_index ) && g_mouse->Pressing() )
					continue;

				m_variable = m_entries[ i ].first;
				m_selected_entry = &m_entries[ i ];
				return true;
			}

			return Hovered();
		}

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 */
		void OnRelease() override {
			m_scrollbar.m_state &= ~kState_Focused;
		}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 * @note This function renders the underlying @p m_scrollbar element.
		 */
		void Render() override {
			const auto position = GetAbsolutePosition();

			// Draw background box
			g_renderer->FillOutlinedRect(
				position.m_x,
				position.m_y,
				m_size.m_x,
				m_size.m_y + 2,
				argb( 45, 45, 45 ),
				argb( 40, 40, 40 )
			);

			// What is the index of the last listbox entry we can draw?
			const auto display_limit = std::min(
				m_first_index + kEntryLimit,
				static_cast< int >( m_entries.size() )
			);

			// Draw listbox entries
			for( auto i = m_first_index; i < display_limit; ++i ) {
				const auto index = static_cast< std::int32_t >( i - m_first_index );
				const auto is_selected = (
					m_selected_entry && ( i == m_selected_entry->first )
				);

				// Draw entry background
				argb background_color( 0 );
				if( is_selected ) {
					background_color = argb( 25, 25, 25 );
				} else if( HoveredEntry( index ) ) {
					background_color = argb( 75, 75, 75 );
				} else if( index % 2 != 0 ) {
					background_color = argb( 60, 60, 60 );
				}

				if( background_color.alpha() != 0 ) {
					g_renderer->Fill(
						position.m_x + 1,
						position.m_y + ( index * kEntryHeight ) + 1,
						m_size.m_x - kScrollbarWidth - 1,
						kEntryHeight,
						background_color
					);
				}

				// Draw entry text
				g_renderer->Text(
					position.m_x + 5,
					position.m_y + ( index * kEntryHeight ) + kEntryHeight / 2 + 1,
					is_selected ? argb( 200, 180, 120 ) : argb( colors::White ),
					kAlign_Left | kAlign_CenterV,
					"%s",
					m_entries[ i ].second.c_str()
				);
			}

			// Draw scrollbar
			m_scrollbar.Render();
		}

		/**
		 * @brief Updates the @ref m_anchor member of the current element.
		 * @param [in] point The new @ref m_anchor point.
		 * @note This function moves the underlying @p m_scrollbar element.
		 */
		void Move( const Vector2D& point ) override {
			// Update current element's anchor
			BaseElement::Move( point );

			// Move the scrollbar alongside the listbox element
			m_scrollbar.Move( point + GetScrollbarPosition() );
		}

		/**
		 * @brief Updates the entries of this listbox instance.
		 * @param [in] entries List of new entries.
		 */
		void Update( const std::vector< std::string >& entries ) {
			m_first_index = 0;
			m_selected_entry = nullptr;

			m_entries.clear();
			m_entries.reserve( entries.size() );
			for( auto i = 0ul; i < entries.size(); ++i )
				m_entries.emplace_back( i, entries[ i ] );

			Initialize();
		}
	};

	/**
	 * @brief Specialized geometry helper for @ref Listbox elements.
	 */
	template<>
	struct ElementGeometry< Listbox > {
		/**
		 * @brief The height of a @ref Listbox element.
		 */
		constexpr static auto kHeight = Listbox::kEntryLimit * Listbox::kEntryHeight;
	};
}

#endif // !LIBSUI_ELEMENTS_LISTBOX_HPP
