/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_MESSAGEBOX_HPP
#define LIBSUI_ELEMENTS_MESSAGEBOX_HPP

#pragma once

#include <vector> // std::vector
#include <functional> // std::function

#include "window.hpp"

#include "../detail/scaled_cutter.hpp"

namespace libsui {
	/**
	 * @brief The alignment policy of the messagebox buttons.
	 */
	enum MessageboxButtonAlignment : std::uint8_t {
		/**
		 * @brief Space should be added around each button, so they can be "centered".
		 */
		kMessagebox_AlignSpaceAround,
		/**
		 * @brief All buttons should be aligned to the right and have a fixed width.
		 */
		kMessagebox_AlignRight
	};

	/**
	 * @brief Type which represents a button choice for a @ref Messagebox element.
	 */
	using MessageboxButtons = std::pair<
		std::string/*text*/,
		std::function< void() >/*on_press*/
	>;

	/**
	 * @brief This class represents a message box (as a new window.)
	 */
	class Messagebox : public Window< BasePanel > {
	private:
		/**
		 * @brief Used for calculating margins inside the groupbox.
		 */
		constexpr static Vector2D kMargin{ 12, 12 };

		/**
		 * @brief Internal type which represents a description line.
		 */
		using DescriptionPair = std::pair< int/*y_offset*/, std::string/*text*/ >;

		/**
		 * @brief Container used to hold all @ref Groupbox child elements.
		 */
		std::vector< std::unique_ptr< Button > > m_children;

		/**
		 * @brief Container used to the messagebox decription as lines.
		 */
		std::vector< DescriptionPair > m_description;

		/**
		 * @brief Pointer to the currently focused child. Otherwise null.
		 */
		Button* m_focused_child = nullptr;

		/**
		 * @brief Callback to be triggered upon closing this messagebox instance.
		 */
		std::function< void( Messagebox* ) > m_on_close;

	public:
		/**
		 * @brief Initializes this @ref Messagebox instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] text The text of this element. Not every element needs a @ref m_text member. This is optional.
		 * @param [in] description Description of this messagebox, where each element represents a new line.
		 * This is so multiline can be dealth with more ease and avoid integration with complex and abstract rendering code.
		 * @param [in] buttons The possible button choices that a user can select.
		 * @param [in] alignment Dictates how the buttons should be aligned.
		 * Whether they should have space around them or should be justified to the right.
		 * @param [in] button_width The minimum/default width of buttons depending on the selected alignment policy.
		 * @param [in] on_close The callback triggered on closing of the messagebox, which will trigger it's deletion
		 * and cleanup from the global state. This argument is REQUIRED or a memory leak will occur!
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Messagebox(
			BaseElement* parent,
			const Vector2D& position,
			const std::string& text,
			const std::vector< std::string >& description,
			std::vector< MessageboxButtons > buttons,
			MessageboxButtonAlignment alignment,
			int button_width,
			std::function< void( Messagebox* ) > on_close
		)
			: Window( parent, position, Vector2D{ 400, 90 }, text )
			, m_on_close( std::move( on_close ) ) {
			auto description_width = 0, description_height = 0;
			for( const auto& line : description ) {
				m_description.emplace_back( description_height, line );

				auto line_width = 0, line_height = 0;
				g_renderer->GetTextSize( line.c_str(), line_width, line_height );

				description_width = std::max( description_width, line_width );
				description_height += line_height;
			}

			// Add this in case something goes horribly wrong.
			if( buttons.empty() )
				buttons.emplace_back( "Cancel", nullptr );

			const auto buttons_count = static_cast< int >( buttons.size() );
			const auto min_button_width = ( ( button_width + kMargin.m_x ) * buttons_count );

			m_size.m_x = ( kMargin.m_x * 2 ) + std::max( description_width, min_button_width );
			m_size.m_y = ( kMargin.m_y * 5 ) + description_height;

			if( alignment == kMessagebox_AlignSpaceAround )
				button_width = ( m_size.m_x - kMargin.m_x * buttons_count ) / buttons_count;

			for( const auto& button : buttons ) {
				const auto index = static_cast< int >( m_children.size() ) + 1;
				const auto x = (
					alignment == kMessagebox_AlignSpaceAround ?
					( kMargin.m_x * index + ( index - 1 ) * button_width ) :
					( m_size.m_x - ( buttons_count - index + 1 ) * button_width )
				);
				m_children.emplace_back( new Button(
					this,
					Vector2D{ x, m_size.m_y - kMargin.m_y - 20 },
					Vector2D{ button_width - kMargin.m_y, 20 },
					button.first,
					button.second
				) );
			}
		}

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 * @note This function invokes underlying child callbacks.
		 */
		bool OnPress() override {
			if( !g_focused_element && g_mouse->Pressing() ) {
				for( auto& child : m_children ) {
					if( child->OnPress() ) {
						// Set this element to focused so we can trigger it's OnRelease callback properly
						// and then later will the OnRelease callback of the proper button get called.
						g_focused_element = this;
						g_focused_element->m_state |= kState_Focused;

						// Keep track of currently focused child. Make sure to mark it as focused.
						m_focused_child = child.get();
						m_focused_child->m_state |= kState_Focused;
						return true;
					}
				}
			}

			return ( Hovered() && g_mouse->Pressing() );
		}

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 */
		void OnRelease() override {
			// Check if saved information is still valid.
			if( !m_focused_child )
				return;

			// This is needed so the focused child can trigger it's release callback.
			// This is due the default CanFocus() method which Button inherits.
			g_focused_element = m_focused_child;

			// If the mouse is hovering over the button hovering, then trigger the release callback.
			// Otherwise, reset the @ref g_focused_element pointer.
			if( g_focused_element->Hovered() ) {
				// Trigger the button's callback function.
				g_focused_element->OnRelease();

				// Deallocate this messagebox instance and remove it from the windows container.
				// NOTE: This also calls the destructor for this messagebox instance.
				return m_on_close( this );
			}

			m_focused_child->m_state &= ~kState_Focused;
			m_focused_child = nullptr;
		}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 */
		void Render() override {
			// Draw header
			Window::Render();

			// Draw message box
			const auto position = GetAbsolutePosition();
			g_renderer->FillOutlinedRect(
				position,
				m_size,
				argb( 65, 65, 65 ),
				argb( 40, 40, 40 )
			);

			for( const auto& line : m_description ) {
				g_renderer->Text(
					position.m_x + kMargin.m_x,
					position.m_y + kMargin.m_y + line.first,
					argb( colors::White ),
					kAlign_Left,
					"%s",
					line.second.c_str()
				);
			}

			// Draw children elements in reverse order,
			// so we can render dropdowns properly
			for( auto& child : ReverseRange( m_children ) )
				child->Render();
		}

		/**
		 * @brief Updates the @ref m_anchor member of the underlying tabs.
		 * @param [in] point The new @ref m_anchor point.
		 * @note This function moves the underlying child elements.
		 */
		void Move( const Vector2D& point ) override {
			// Update current element's anchor
			Window::Move( point );

			// Move all elements
			for( auto& child : m_children )
				child->Move( point );
		}
	};
}
#endif // !LIBSUI_ELEMENTS_MESSAGEBOX_HPP
