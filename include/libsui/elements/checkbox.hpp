/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_CHECKBOX_HPP
#define LIBSUI_ELEMENTS_CHECKBOX_HPP

#pragma once

#include "base_element.hpp"

namespace libsui {
	/**
	 * @brief This class represents a checkbox element.
	 */
	class Checkbox : public BaseElement {
	private:
		/**
		 * @brief Variable to be used for interacting with this element.
		 */
		bool& m_variable;

	public:
		/**
		 * @brief Initializes this @ref Checkbox instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] size The clickable size of the current element.
		 * @param [in] text The text of this element. Not every element needs a @ref m_text member. This is optional.
		 * @param [in,out] variable The variable which should be modified on value change.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Checkbox(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size,
			const std::string& text,
			bool& variable
		)
			: BaseElement( parent, position, size, text, kType_Checkbox )
			, m_variable( variable ) {}

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 */
		bool OnPress() override {
			if( g_mouse->Pressed() ) {
				m_variable = !m_variable;
				return true;
			}

			return false;
		}

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 */
		void OnRelease() override {}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 */
		void Render() override {
			const auto center_offset = GetOffset( kOffset_Center );
			const auto position = GetAbsolutePosition();

			// Draw box
			g_renderer->FillOutlinedRect(
				center_offset,
				position.m_y,
				m_size.m_y,
				m_size.m_y,
				argb( 45, 45, 45 ),
				Hovered() ? argb( 180, 150, 115 ) : argb( 40, 40, 40 )
			);

			if( m_variable ) {
				// From upper left to lower right
				g_renderer->Line(
					center_offset + 2,
					position.m_y + 2,
					center_offset + m_size.m_y - 2 - 1,
					position.m_y + m_size.m_y - 2 - 1,
					argb( 100, 100, 100 )
				);

				// From lower left to upper right
				g_renderer->Line(
					center_offset + m_size.m_y - 2 - 1,
					position.m_y + 2,
					center_offset + 2,
					position.m_y + m_size.m_y - 2 - 1,
					argb( 100, 100, 100 )
				);
			}

			// Draw @ref m_text
			g_renderer->Text(
				position.m_x,
				position.m_y + m_size.m_y / 2,
				argb( colors::White ),
				kAlign_Left | kAlign_CenterV,
				"%s",
				m_text.c_str()
			);
		}
	};
}

#endif // !LIBSUI_ELEMENTS_CHECKBOX_HPP
