/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_TAB_HPP
#define LIBSUI_ELEMENTS_TAB_HPP

#pragma once

#include "base_element.hpp"
#include "groupbox.hpp"
#include "panel.hpp"

namespace libsui {
	/**
	 * @brief This class represents a tab button, acompanied by an underlying @ref Panel element.
	 */
	template< class T >
	class Tab : public BaseElement {
	private:
		/**
		 * @brief The default tab height.
		 */
		constexpr static auto kTabHeight = 25;

		/**
		 * @brief Child element in charge of the layout and size of it's child elements.
		 */
		std::unique_ptr< T > m_panel = nullptr;

	public:
		/**
		 * @brief Initializes this @ref Tab instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] panel_size The clickable size of the @ref Panel element.
		 * @param [in] text The text of this element. Not every element needs a @ref m_text member. This is optional.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Tab(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& panel_size,
			const std::string& text
		)
			: BaseElement( parent, position, { 0, kTabHeight }, text, kType_Tab )
			, m_panel( std::make_unique< T >( parent, Vector2D{ 0, position.m_y + kTabHeight }, panel_size ) ) {}

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @note This function invokes the callback of the underlying @ref Panel element.
		 */
		bool OnPress() override {
			return m_panel->OnPress();
		}

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 * @note This function invokes the callback of the underlying @ref Panel element.
		 */
		void OnRelease() override {
			m_panel->OnRelease();
		}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 * @note This function renders the underlying @ref Panel element.
		 */
		void Render() override {
			const auto position = GetAbsolutePosition();

			// Draw text placeholder box
			g_renderer->FillOutlinedRect(
				position,
				m_size,
				argb( 45, 45, 45 ),
				Hovered() ? argb( 180, 150, 115 ) : argb( 40, 40, 40 )
			);

			if( m_state & kState_Focused ) {
				g_renderer->Line(
					position.m_x + 1,
					position.m_y + m_size.m_y - 1,
					position.m_x + m_size.m_x - 2,
					position.m_y + m_size.m_y - 1,
					argb( 180, 150, 115 )
				);

				g_renderer->Gradient(
					position + 1,
					m_size - 2,
					argb( colors::Black, 0 ),
					argb( colors::White, 20 )
				);

				m_panel->Render();
			} else {
				g_renderer->Gradient(
					position + 1,
					m_size - 2,
					argb( colors::Black, 0 ),
					argb( colors::Black, 30 )
				);
			}

			// Draw @ref m_text
			g_renderer->Text(
				position.m_x + m_size.m_x / 2,
				position.m_y + m_size.m_y / 2,
				argb( colors::White ),
				kAlign_CenterH | kAlign_CenterV,
				"%s",
				m_text.c_str()
			);
		}

		/**
		 * @brief Updates the @ref m_anchor member of the current element.
		 * @param [in] point The new @ref m_anchor point.
		 * @note This function also moves the underlying @p m_panel element.
		 */
		void Move( const Vector2D& point ) override {
			// Update current element's anchor
			BaseElement::Move( point );

			// Move the underlying @p m_panel element
			m_panel->Move( point );
		}

		/**
		 * @brief Creates and adds a new @ref Groupbox instance to the @p m_panel element.
		 * @param [in] text Text of the new @ref Groupbox.
		 * @param [in] position Starting position. Ignored for @ref AutoPanel.
		 * @param [in] size Groupbox size. Ignored for @ref AutoPanel.
		 * @param [in] policy Policy of this groupbox. Ignored for @ref Panel.
		 * @return Pointer to the newly created @ref Groupbox.
		 * @note Wrapper for @ref Panel underlying function.
		 */
		inline Groupbox* Add(
			const std::string& text,
			const Vector2D& position = {},
			const Vector2D& size = {},
			GroupPolicy_t policy = kPolicy_Default
		) {
			return m_panel->Add( text, position, size, policy );
		}

		/**
		 * @brief Recomputes the position and size of the current element.
		 * @param [in] width Width of the current tab element.
		 * @note This function also recomputes the size of the @p m_panel member.
		 */
		inline void RecomputeGeometry( std::int32_t width ) {
			// Update tab width and position
			m_size.m_x = width;
			m_position.m_x *= width;

			// Recompute @p m_panel size
			m_panel->RecomputeGeometry();
		}

		/**
		 * @brief Gets the total height of this element.
		 * @return Total height of this element.
		 */
		[[nodiscard]] inline std::int32_t GetTotalHeight() const {
			return m_size.m_y + m_panel->m_size.m_y;
		}

		/**
		 * @brief Gets a pointer to the underlying panel element.
		 * @return Pointer to the underlying panel element.
		 */
		inline T* GetPanel() {
			return m_panel.get();
		}
	};
}

#endif // !LIBSUI_ELEMENTS_TAB_HPP
