/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_DROPDOWN_HPP
#define LIBSUI_ELEMENTS_DROPDOWN_HPP

#pragma once

#include "base_element.hpp"

#include <vector>

namespace libsui {
	/**
	 * @brief This class represents a single-choice dropdown element.
	 */
	class Dropdown : public BaseElement {
	private:
		/**
		 * @brief Internal type used to represent a dropdown entry.
		 */
		using Entry = std::pair< int, std::string >;

		/**
		 * @brief Internal type used to hold all dropdown entries.
		 */
		using EntryContainer = std::vector< Entry >;

		/**
		 * @brief Indicates whether this dropdown is open.
		 */
		bool m_open = false;

		/**
		 * @brief Variable to be used for interacting with this element.
		 */
		int& m_variable;

		/**
		 * @brief Currently selected entry.
		 * @note nullptr if no entry is selected.
		 */
		Entry* m_selected_entry = nullptr;

		/**
		 * @brief List to hold all dropdown entries.
		 */
		EntryContainer m_entries;

	private:
		/**
		 * @brief Acquires element's height based on current state.
		 * @return Element's height based on state, in pixels.
		 */
		[[nodiscard]] std::int32_t GetHeight() const {
			if( !m_open )
				return m_size.m_y;

			return ( static_cast< std::int32_t >( m_entries.size() ) * m_size.m_y );
		}

		/**
		 * @brief Determines whether the current entry is hovered.
		 * @param [in] index Index of the current element in the @ref m_entries container.
		 * @note This function is only invoked when the dropdown menu is open.
		 * @return true, if mouse is hovering the current element
		 * @return false, if mouse is not hovering the current element
		 */
		[[nodiscard]] bool HoveredEntry( const std::uintptr_t index ) const {
			if( !CanFocus() )
				return false;

			const auto position = GetAbsolutePosition();
			return g_mouse->Over(
				position.m_x + m_size.m_x / 2,
				position.m_y + static_cast< std::int32_t >( index ) * m_size.m_y,
				m_size.m_x / 2,
				m_size.m_y
			);
		}

	public:
		/**
		 * @brief Initializes this @ref Dropdown instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] size The clickable size of the current element.
		 * @param [in] text The text of this element. Not every element needs a @ref m_text member. This is optional.
		 * @param [in,out] variable The variable which should be modified on value change.
		 * @param [in] entries Indexed entry list that should populate this element.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Dropdown(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size,
			const std::string& text,
			int& variable,
			EntryContainer entries
		)
			: BaseElement( parent, position, size, text, kType_Dropdown )
			, m_open( false )
			, m_variable( variable )
			, m_entries( std::move( entries ) ) {
			for( auto& entry : m_entries ) {
				if( variable == entry.first ) {
					m_selected_entry = &entry;
					break;
				}
			}
		}

		/**
		 * @brief Initializes this @ref Dropdown instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] size The clickable size of the current element.
		 * @param [in] text The text of this element. Not every element needs a @ref m_text member. This is optional.
		 * @param [in,out] variable The variable which should be modified on value change.
		 * @param [in] entries List of entries that should populate this element.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Dropdown(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size,
			const std::string& text,
			int& variable,
			const std::vector< std::string >& entries
		)
			: BaseElement( parent, position, size, text, kType_Dropdown )
			, m_open( false )
			, m_variable( variable ) {
			m_entries.reserve( entries.size() );
			for( auto i = 0ul; i < entries.size(); ++i )
				m_entries.emplace_back( i, entries[ i ] );

			const auto list_size = static_cast< int >( entries.size() );
			if( variable >= 0 && variable < list_size )
				m_selected_entry = &m_entries[ variable ];
		}

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 */
		bool OnPress() override {
			// Don't handle any element logic, unless the dropdown menu is open
			if( !m_open ) {
				// Open dropdown panel only if there are entries
				if( !m_entries.empty() )
					m_open = true;
				return true;
			}

			for( auto i = 0u; i < m_entries.size(); ++i ) {
				if( !HoveredEntry( i ) )
					continue;

				m_open = false;
				m_variable = m_entries[ i ].first;
				m_selected_entry = &m_entries[ i ];

				return true;
			}

			return false;
		}

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 */
		void OnRelease() override {}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 */
		void Render() override {
			const auto position = GetAbsolutePosition();

			// Draw @ref m_text
			g_renderer->Text(
				position.m_x,
				position.m_y + m_size.m_y / 2,
				argb( colors::White ),
				kAlign_Left | kAlign_CenterV,
				"%s",
				m_text.c_str()
			);

			const auto offset_left = GetOffset( kOffset_Left );

			// Draw placeholder box for the current variable's name
			g_renderer->FillOutlinedRect(
				offset_left,
				position.m_y,
				m_size.m_x / 2,
				GetHeight(),
				argb( 45, 45, 45 ),
				( Hovered() && !m_open ) ? argb( 180, 150, 115 ) : argb( 40, 40, 40 )
			);

			// Draw current variable name
			if( m_selected_entry && !m_open ) {
				// \ line
				g_renderer->Line(
					offset_left + m_size.m_x / 2 - 12,
					position.m_y + m_size.m_y / 2 - 1,
					offset_left + m_size.m_x / 2 - 9,
					position.m_y + m_size.m_y / 2 + 2,
					argb( colors::White )
				);

				// / line
				g_renderer->Line(
					offset_left + m_size.m_x / 2 - 6,
					position.m_y + m_size.m_y / 2 - 1,
					offset_left + m_size.m_x / 2 - 9,
					position.m_y + m_size.m_y / 2 + 2,
					argb( colors::White )
				);

				g_renderer->Text(
					offset_left + 5,
					position.m_y + m_size.m_y / 2,
					argb( colors::White ),
					kAlign_Left | kAlign_CenterV,
					"%s",
					m_selected_entry->second.c_str()
				);
				return;
			}

			// Don't draw the entry list if the dropdown isn't open
			if( !m_open )
				return;

			// Draw dropdown list
			for( auto i = 0ul; i < m_entries.size(); ++i ) {
				auto is_selected = false;
				if( m_selected_entry )
					is_selected = ( m_entries[ i ].first == m_selected_entry->first );

				// Highlight currently hovered entry
				argb background_color( 0 );
				if( is_selected ) {
					background_color = argb( 25, 25, 25 );
				} else if( HoveredEntry( i ) ) {
					background_color = argb( 60, 60, 60 );
				}

				if( background_color.alpha() != 0 ) {
					g_renderer->Fill(
						offset_left + 1,
						position.m_y + ( i * m_size.m_y ) + 1,
						m_size.m_x / 2 - 2,
						m_size.m_y - 2,
						background_color
					);
				}

				// Draw entry text
				g_renderer->Text(
					offset_left + 5,
					position.m_y + ( i * m_size.m_y ) + m_size.m_y / 2,
					is_selected ? argb( 180, 150, 115 ) : argb( colors::White ),
					kAlign_Left | kAlign_CenterV,
					"%s",
					m_entries[ i ].second.c_str()
				);
			}
		}

		/**
		 * @brief Determines whether we're hovering over the current element.
		 * @return true, if mouse is hovering the element.
		 * @return false, if mouse is not hovering the element.
		 */
		bool Hovered() override {
			if( !CanFocus() )
				return false;

			const auto position = GetAbsolutePosition();
			return g_mouse->Over(
				position.m_x + m_size.m_x / 2,
				position.m_y,
				m_size.m_x / 2,
				GetHeight()
			);
		}
	};
}

#endif // !LIBSUI_ELEMENTS_DROPDOWN_HPP
