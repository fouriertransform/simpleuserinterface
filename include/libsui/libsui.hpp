/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_LIBSUI_HPP
#define LIBSUI_LIBSUI_HPP

#pragma once

// Helper classes
#include "detail/argb.hpp"
#include "detail/context.hpp"
#include "detail/vector2d.hpp"
#include "detail/mouse_interface.hpp"
#include "detail/renderer_interface.hpp"
#include "detail/reverse_iterator.hpp"

// Elements
#include "elements/button.hpp"
#include "elements/checkbox.hpp"
#include "elements/dropdown.hpp"
#include "elements/groupbox.hpp"
#include "elements/listbox.hpp"
#include "elements/panel.hpp"
#include "elements/auto_panel.hpp"
#include "elements/scrollbar.hpp"
#include "elements/slider.hpp"
#include "elements/tab.hpp"
#include "elements/window.hpp"
#include "elements/messagebox.hpp"

namespace libsui {
	/**
	 * @brief The main menu class, that manages @ref Window behavior.
	 */
	class Menu {
	private:
		/**
		 * @brief Indicates how much this window moved during dragging.
		 */
		Vector2D m_mouse_delta{};

		/**
		 * @brief Container used to hold all child windows.
		 */
		std::vector< std::unique_ptr< BaseElement > > m_windows;

		/**
		 * @brief Window currently being dragged, if any.
		 */
		BaseElement* m_dragged_window = nullptr;

	public:
		/**
		 * @brief Default @ref Menu constructor.
		 */
		Menu() = default;

		/**
		 * @brief Constructs and adds a new @ref Window to the @p m_windows container.
		 * @param [in] position The absolute starting position of the new window.
		 * @param [in] size The size of the new window.
		 * @param [in] name The name of the new window.
		 * @return The pointer to the newly created @p Window instance.
		 */
		template< class T >
		inline Window< T >* Add(
			const Vector2D& position,
			const Vector2D& size,
			const std::string& name
		) {
			auto* element = new Window< T >( nullptr, position, size, name );
			( void )m_windows.emplace_back( element );
			return element;
		}

		/**
		 * @brief Creates a @ref Messagebox instance and appends it to the internal window list.
		 * @param [in] position The starting position.
		 * @param [in] text The title.
		 * @param [in] description The description, delimited as lines.
		 * @param [in] buttons The button options.
		 * @param [in] alignment The alignment of the buttons.
		 * @param [in] button_width The fixed button width for the case of right-alignment.
		 * @return Pointer to the newly created messagebox instance.
		 */
		inline Messagebox* Notify(
			const Vector2D& position,
			const std::string& text,
			const std::vector< std::string >& description,
			const std::vector< MessageboxButtons >& buttons,
			MessageboxButtonAlignment alignment = kMessagebox_AlignRight,
			int button_width = 65
		) {
			// Unfocus all windows
			for( auto& window : m_windows )
				window->m_state &= ~kState_Focused;

			// Emplace to front, to make the window render properly
			auto* element = new Messagebox(
				nullptr,
				position,
				text,
				description,
				buttons,
				alignment,
				button_width,
				[ this ]( Messagebox* pointer ) {
					for( auto it = m_windows.begin(); it != m_windows.end(); ++it ) {
						if( static_cast< void* >( it->get() ) == pointer )
							return static_cast< void >( m_windows.erase( it ) );
					}
				}
			);

			// Give focus to newly created messagebox
			element->m_state |= kState_Focused;

			// Add to windows list and return pointer to newly created messagebox
			( void )m_windows.emplace( m_windows.begin(), element );
			return element;
		}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 * @note This function renders the underlying @ref Window elements.
		 */
		void Render() {
			for( auto& window : ReverseRange( m_windows ) )
				window->Render();
		}

		/**
		 * @brief Handles window input, alongside window focusing and dragging.
		 */
		void HandleInput() {
			constexpr static auto reset_focus = []() {
				g_focused_element->OnRelease();
				g_focused_element->m_state &= ~kState_Focused;
				g_focused_element = nullptr;
			};

			const auto pressing_mouse = g_mouse->Pressing();
			const auto scrolling_event = ( g_mouse->m_scroll_direction != kScroll_None );
			const auto mouse_event = ( pressing_mouse || scrolling_event );

			// If a window has consumed user input, then refocus.
			if( !pressing_mouse ) {
				// If we aren't pressing and we had a window that
				// we dragged last frame, then forget about the window,
				// as we aren't dragging anymore.
				if( m_dragged_window ) {
					m_dragged_window->m_state &= ~kState_Dragged;
					m_dragged_window = nullptr;
				}

				// If we aren't consuming a mouse event, reset focused element
				if(
					g_focused_element &&
					( g_focused_element->m_type != kType_Textbox || scrolling_event )
				) {
					reset_focus();
				}
			} else {
				if( g_focused_element && g_focused_element->m_type == kType_Textbox )
					reset_focus();
			}

			if( g_focused_element ) {
				// Handle the slider @ref m_focused_element @ref OnPress event continuously,
				// even when out of clickable area
				const auto has_slider = (
					( g_focused_element->m_type == kType_Slider ) ||
					( g_focused_element->m_type == kType_Listbox )
				);
				// If we're interacting with a sliding element, don't proceed further.
				if( has_slider && g_focused_element->OnPress() )
					return;
			}

			// Handle window logic
			auto found_hovered_window = false;
			for(
				auto it = m_windows.begin();
				!m_dragged_window && !g_focused_element && it != m_windows.end();
				++it
			) {
				auto* window = it->get();

				// Is the current window header hovered?
				const auto header_hovered = g_mouse->Over(
					window->m_position.m_x,
					window->m_position.m_y - Window< Panel >::kHeaderHeight,
					window->m_size.m_x,
					Window< Panel >::kHeaderHeight
				);

				// Update hovering state for every window
				if( !found_hovered_window && ( header_hovered || window->Hovered() ) ) {
					window->m_state |= kState_Interactable;
					found_hovered_window = true;
				} else {
					window->m_state &= ~kState_Interactable;
				}

				// Only acquire a new window to drag, when:
				// - We aren't dragging a window currently
				// - The header of the window is hovered
				// - We are pressing with our mouse
				// - There is no element currently focused
				if( header_hovered && pressing_mouse ) {
					// Remove focus from the most recently focused window
					m_windows.front()->m_state &= ~kState_Focused;

					// Track which window we're dragging
					// and set internal state accordingly
					m_dragged_window = window;
					m_dragged_window->m_state |= ( kState_Dragged | kState_Focused );

					m_mouse_delta = g_mouse->m_position - m_dragged_window->m_position;

					// Transfer focus to the current window
					std::rotate( m_windows.begin(), it, std::next( it ) );
					break;
				}

				if( found_hovered_window && mouse_event && window->OnPress() ) {
					// If the window which consumed mouse input isn't focused, then refocus.
					if( !( window->m_state & kState_Focused ) ) {
						// Remove focus from the most recently focused window
						m_windows.front()->m_state &= ~kState_Focused;

						// Set focus on new window
						window->m_state |= kState_Focused;

						// Transfer focus to the current window
						std::rotate( m_windows.begin(), it, std::next( it ) );
					}
					break;
				}
			}

			if( m_dragged_window ) {
				// If we are dragging a window, move it to it's new position
				m_dragged_window->m_position = g_mouse->m_position - m_mouse_delta;
				m_dragged_window->Move( m_dragged_window->m_position );
			}
		}
	};
}

#endif // !LIBSUI_LIBSUI_HPP
