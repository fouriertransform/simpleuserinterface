/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_WINDOW_HPP
#define LIBSUI_ELEMENTS_WINDOW_HPP

#pragma once

#include <memory> // std::unique_ptr
#include <vector> // std::vector
#include <functional> // std::function
#include <initializer_list> // std::initializer_list

#include "tab.hpp"

namespace libsui {
	/**
	 * @brief This class represents a window.
	 * @note The @ref m_anchor member is not used here!
	 * @ref m_position refers to the absolute position in this case!
	 */
	template< class T >
	class Window : public BaseElement {
	public:
		/**
		 * @brief Height of the header above the Window, where @ref m_text is rendered.
		 */
		constexpr static auto kHeaderHeight = 20;

		/**
		 * @brief Container type for keeping child elements.
		 */
		using Container = std::vector< std::unique_ptr< Tab< T > > >;

		/**
		 * @brief Callback that is invoked on instantiation.
		 * Used for populating child tabs.
		 */
		using SetupCallback = std::function< void( Container& ) >;

	private:
		/**
		 * @brief Index of the currently selected tab in @ref m_tabs.
		 */
		std::uintptr_t m_current_tab = 0;

		/**
		 * @brief Container used to hold all tabs.
		 */
		Container m_tabs;

	public:
		/**
		 * @brief Initializes this @ref Window instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] size The clickable size of the current element.
		 * @param [in] text The text of this element. Not every element needs a @ref m_text member. This is optional.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Window(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size,
			const std::string& text
		)
			: BaseElement( parent, position, size, text, kType_Window ) {}

		/**
		 * @brief Setups the current instance's tabs and elements.
		 * @param [in] tabs List of tabs.
		 * @param [in] setup_callback User callback for adding elements.
		 */
		void Setup(
			const std::initializer_list< std::string >& tabs,
			const SetupCallback& setup_callback
		) {
			const auto tab_count = static_cast< int >( tabs.size() );
			if( !tab_count )
				return;

			// Add tabs
			for( auto i = 0; i < tab_count; ++i ) {
				m_tabs.emplace_back( new Tab< T >(
					this,
					{ i, 0 },
					{ m_size.m_x, m_size.m_y - kHeaderHeight },
					*( tabs.begin() + i )
				) );
			}

			// Set focus on the first tab
			m_tabs.front()->m_state |= kState_Focused;

			// Add elements
			setup_callback( m_tabs );

			// Resize window, if necessary
			const auto width = m_size.m_x / tab_count;
			for( auto& tab : m_tabs )
				tab->RecomputeGeometry( width );

			// Set initial surface size
			m_size.m_y = m_tabs.front()->GetTotalHeight();
		}

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 * @note This function invokes underlying child callbacks.
		 */
		bool OnPress() override {
			// Handle tab logic
			const auto dragged = ( m_state & kState_Dragged );
			if( g_mouse->Pressing() && !dragged && !g_focused_element ) {
				for( auto i = 0u; i < m_tabs.size(); ++i ) {
					if( !m_tabs[ i ]->Hovered() )
						continue;

					// Update window size (because panel sizes vary across tabs)
					m_size.m_y = m_tabs[ i ]->GetTotalHeight();

					// Unset the old current tab
					m_tabs[ m_current_tab ]->m_state &= ~kState_Focused;

					// Set focused state for the new current tab
					m_tabs[ m_current_tab = i ]->m_state |= kState_Focused;
					return true;
				}
			}

			// Handle child callbacks
			return m_tabs[ m_current_tab ]->OnPress();
		}

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 */
		void OnRelease() override {}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 */
		void Render() override {
			// Draw header
			g_renderer->Fill(
				m_position.m_x,
				m_position.m_y - kHeaderHeight,
				m_size.m_x,
				kHeaderHeight,
				( m_state & kState_Focused ) ? argb( 25, 25, 25 ) : argb( 45, 45, 45 )
			);

			// Draw @ref m_text
			g_renderer->Text(
				m_position.m_x + 5,
				m_position.m_y - 10,
				argb( colors::White ),
				kAlign_Left | kAlign_CenterV,
				"%s",
				m_text.c_str()
			);

			// Render tab buttons
			for( const auto& tab : m_tabs )
				tab->Render();
		}

		/**
		 * @brief Updates the @ref m_anchor member of the underlying tabs.
		 * @param [in] point The new @ref m_anchor point.
		 * @note This function moves the underlying child elements.
		 */
		void Move( const Vector2D& point ) override {
			// Move all elements alongside the anchor point
			for( auto& tabs : m_tabs )
				tabs->Move( point );
		}

		/**
		 * @brief Determines whether we're hovering over the current element.
		 * @return true, if mouse is hovering the element.
		 * @return false, if mouse is not hovering the element.
		 */
		bool Hovered() override {
			return g_mouse->Over(
				m_position.m_x,
				m_position.m_y,
				m_size.m_x,
				m_size.m_y
			);
		}

		/**
		 * @brief Indicates whether this element can be interected with.
		 * @return true, if this element can be interacted with.
		 * @return false, if this element cannot be interacted with.
		 */
		[[nodiscard]] bool IsInteractable() const override {
			return ( m_state & kState_Interactable );
		}
	};
}
#endif // !LIBSUI_ELEMENTS_WINDOW_HPP
