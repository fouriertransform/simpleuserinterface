/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_DETAIL_VECTOR2D_HPP
#define LIBSUI_DETAIL_VECTOR2D_HPP

#pragma once

#include <cstdint> // std::int32_t

namespace libsui {
	/**
	 * @brief This class represents a 2-dimensional integer vector.
	 */
	struct Vector2D {
		constexpr Vector2D() = default;

		constexpr Vector2D( std::int32_t x, std::int32_t y ) noexcept
			: m_x( x )
			, m_y( y ) {}

		constexpr Vector2D& operator+=( const Vector2D& other ) noexcept {
			m_x += other.m_x; m_y += other.m_y;
			return *this;
		}

		constexpr Vector2D& operator-=( const Vector2D& other ) noexcept {
			m_x -= other.m_x; m_y -= other.m_y;
			return *this;
		}

		constexpr Vector2D& operator*=( std::int32_t value ) noexcept {
			m_x *= value; m_y *= value;
			return *this;
		}

		constexpr Vector2D& operator*=( const Vector2D& other ) noexcept {
			m_x *= other.m_x; m_y *= other.m_y;
			return *this;
		}

		constexpr Vector2D& operator/=( const Vector2D& other ) noexcept {
			m_x /= other.m_x; m_y /= other.m_y;
			return *this;
		}

		// this ought to be an opcode.
		constexpr Vector2D& operator+=( std::int32_t value ) noexcept {
			m_x += value; m_y += value;
			return *this;
		}

		// this ought to be an opcode.
		constexpr Vector2D& operator/=( std::int32_t value ) noexcept {
			m_x /= value; m_y /= value;
			return *this;
		}

		constexpr Vector2D& operator-=( std::int32_t value ) noexcept {
			m_x -= value; m_y -= value;
			return *this;
		}

		constexpr Vector2D operator-( const Vector2D& other ) const noexcept {
			return { m_x - other.m_x, m_y - other.m_y };
		}

		constexpr Vector2D operator-( std::int32_t value ) const noexcept {
			return { m_x - value, m_y - value };
		}

		constexpr Vector2D operator+( const Vector2D& other ) const noexcept {
			return { m_x + other.m_x, m_y + other.m_y };
		}

		constexpr Vector2D operator+( std::int32_t value ) const noexcept {
			return { m_x + value, m_y + value };
		}

		constexpr Vector2D operator/( const std::int32_t other ) const noexcept {
			return { m_x / other, m_y / other };
		}

		[[nodiscard]] constexpr bool Empty() const noexcept {
			return ( m_x == 0 && m_y == 0 );
		}

		std::int32_t m_x = 0, m_y = 0;
	};
}

#endif // !LIBSUI_DETAIL_VECTOR2D_HPP
