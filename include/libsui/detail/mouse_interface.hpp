/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_DETAIL_MOUSE_INTERFACE_HPP
#define LIBSUI_DETAIL_MOUSE_INTERFACE_HPP

#pragma once

#include <cstdint> // std::int32_t, std::uintptr_t, std::uint8_t

#include "vector2d.hpp"

namespace libsui {
	/**
	 * @brief Describes the direction of a mouse scrolling event.
	 */
	enum ScrollDirection_t : std::uint8_t {
		/**
		 * @brief Nothing happened. Blank state.
		 */
		kScroll_None,
		/**
		 * @brief We've scrolled down this frame.
		 */
		kScroll_Down,
		/**
		 * @brief We've scrolled up this frame.
		 */
		kScroll_Up
	};

	/**
	 * @brief Interface for implementing a mouse event wrapper.
	 */
	struct IMouse {
		/**
		 * @brief Default virtual destructor, which can be overloaded to aid
		 * in safely calling delete on this object in case of dynamic allocation.
		 */
		virtual ~IMouse() = default;

		/**
		 * @brief Should render the mouse pointer.
		 */
		virtual void Render() = 0;

		/**
		 * @brief Should determine whether the mouse is currently being pressed on.
		 * @return true, if the mouse is currently being pressed
		 * @return false, if the mouse is currently not being pressed
		 */
		virtual bool Pressing() = 0;

		/**
		 * @brief Should determine whether the mouse had been pressed at most 250ms ago.
		 * @return
		 * true, if the mouse was pressed at most 250ms ago
		 * false, if the mouse was not pressed at most 250ms ago
		 */
		virtual bool Pressed() = 0;

		/**
		 * @brief Should determine whether the mouse is hovering the given area.
		 * @param [in] x Starting point on the X axis.
		 * @param [in] y Starting point on the Y axis.
		 * @param [in] w Width of the area.
		 * @param [in] h Height of the area.
		 * @return
		 * true, if mouse is hovering the area
		 * false, if mouse is not hovering the area
		 */
		virtual bool Over(
			std::int32_t x,
			std::int32_t y,
			std::int32_t w,
			std::int32_t h
		) = 0;

		/**
		 * @brief The position of the cursor in it's focal point.
		 */
		Vector2D m_position;

		/**
		 * @brief Mouse scroll state for the current frame.
		 */
		std::uint8_t m_scroll_direction = kScroll_None;

		/**
		 * @brief Mouse scroll amount for the current frame.
		 */
		int m_scroll_amount = 0;
	};
}

#endif // !LIBSUI_DETAIL_MOUSE_INTERFACE_HPP
