/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_DETAIL_CONTEXT_HPP
#define LIBSUI_DETAIL_CONTEXT_HPP

#pragma once

#include "renderer_interface.hpp"
#include "mouse_interface.hpp"

namespace libsui {
	/**
	 * @brief Global pointer to the rendering engine implementation.
	 */
	inline IRenderer* g_renderer = nullptr;

	/**
	 * @brief Global pointer to the mouse event handler implementation.
	 */
	inline IMouse* g_mouse = nullptr;

	/**
	 * @brief Global pointer to the currently focused element.
	 * @note Only one element can be focused at a time.
	 */
	inline struct BaseElement* g_focused_element = nullptr;

	/**
	 * @brief Initializes the global context for the interface engines.
	 * @param [in] renderer The pointer to the rendering engine implementation.
	 * @param [in] mouse The pointer to the mouse event handler implementation.
	 */
	inline void Setup( IRenderer* renderer, IMouse* mouse ) {
		g_renderer = renderer;
		g_mouse = mouse;
		g_focused_element = nullptr;
	}
}

#endif // !LIBSUI_DETAIL_CONTEXT_HPP
