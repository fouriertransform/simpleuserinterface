/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_DETAIL_SCALED_CUTTER_HPP
#define LIBSUI_DETAIL_SCALED_CUTTER_HPP

#pragma once

#include <cstdint> // std::int32_t
#include <cmath> // std::min, std::max

#include "vector2d.hpp"

namespace libsui {
	/**
	 * @brief This class allows for cutting of a given area in scaled units.
	 */
	class ScaledCutter {
	private:
		Vector2D m_start, m_end;

		[[nodiscard]] inline constexpr auto GetOffset( int size, int percentage ) const -> int {
			return static_cast< int >( size * ( percentage / 100.f ) );
		}

	public:
		ScaledCutter( const Vector2D& start, const Vector2D& end )
			: m_start( start )
			, m_end( end ) {}

		[[nodiscard]] auto Top( const int percentage ) const -> ScaledCutter {
			const auto size = m_end.m_y - m_start.m_y;
			const auto y = std::min( m_end.m_y, m_start.m_y + GetOffset( size, percentage ) );
			return ScaledCutter( m_start, Vector2D{ m_end.m_x, y } );
		}

		[[nodiscard]] auto Right( const int percentage ) const -> ScaledCutter {
			const auto size = m_end.m_x - m_start.m_x;
			const auto x = std::max( m_start.m_x, m_end.m_x - GetOffset( size, percentage ) );
			return ScaledCutter( Vector2D{ x, m_start.m_y }, m_end );
		}

		[[nodiscard]] auto Bottom( const int percentage ) const -> ScaledCutter {
			const auto size = m_end.m_y - m_start.m_y;
			const auto y = std::max( m_start.m_y, m_end.m_y - GetOffset( size, percentage ) );
			return ScaledCutter( Vector2D{ m_start.m_x, y }, m_end );
		}

		[[nodiscard]] auto Left( const int percentage ) const -> ScaledCutter {
			const auto size = m_end.m_x - m_start.m_x;
			const auto x = std::min( m_end.m_x, m_start.m_x + GetOffset( size, percentage ) );
			return ScaledCutter( m_start, Vector2D{ x, m_end.m_y } );
		}

		[[nodiscard]] inline auto GetStart() const -> const Vector2D& {
			return m_start;
		}

		[[nodiscard]] inline auto GetEnd() const -> const Vector2D& {
			return m_end;
		}

		[[nodiscard]] inline auto GetSize() const -> Vector2D {
			return m_end - m_start;
		}
	};
}

#endif // !LIBSUI_DETAIL_SCALED_CUTTER_HPP
