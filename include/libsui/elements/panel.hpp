/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_PANEL_HPP
#define LIBSUI_ELEMENTS_PANEL_HPP

#pragma once

#include "base_element.hpp"
#include "base_panel.hpp"

#include "../detail/scaled_cutter.hpp"

#include <limits> // std::numeric_limits< std::int32_t >::max()

namespace libsui {
	/**
	 * @brief This class represents a panel-like element, which can be "cut" in either .
	 * This class also has the ability to increase it's height if
	 * the furthest point of an element exceeds the panel's furthest point.
	 */
	class Panel : public BasePanel {
	private:
		/**
		 * @brief Container used to hold all @ref Groupbox child elements.
		 */
		std::vector< std::unique_ptr< Groupbox > > m_children;

	public:
		/**
		 * @brief Initializes this @ref Panel instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position A point which is relative to @ref m_anchor.
		 * @param [in] size The size of the current element.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Panel(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size
		)
			: BasePanel( parent, position, size ) {}

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 * @note This function invokes underlying child callbacks.
		 */
		bool OnPress() override {
			for( auto& child : m_children ) {
				if( child->OnPress() )
					return true;
			}

			return ( Hovered() && g_mouse->Pressing() );
		}

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 * @note This function invokes underlying child callbacks.
		 */
		void OnRelease() override {
			for( auto& child : m_children )
				child->OnRelease();
		}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 * @note This function renders the underlying @ref Groupbox elements.
		 */
		void Render() override {
			// Draw window
			g_renderer->FillOutlinedRect(
				GetAbsolutePosition(),
				m_size,
				argb( 65, 65, 65 ),
				argb( 40, 40, 40 )
			);

			// Draw children elements in reverse order,
			// so we can render dropdowns properly
			for( auto& child : ReverseRange( m_children ) )
				child->Render();
		}

		/**
		 * @brief Updates the @ref m_anchor member of the current element.
		 * @param [in] point The new @ref m_anchor point.
		 * @note This function moves the underlying child elements.
		 */
		void Move( const Vector2D& point ) override {
			// Update current element's anchor
			BaseElement::Move( point );

			// Move all elements
			for( auto& child : m_children )
				child->Move( m_position + point );
		}

		/**
		 * @brief Recomputes the size of the current element and adjusts the height, if necessary.
		 */
		void RecomputeGeometry() override {
			if( m_children.empty() )
				return;

			for( const auto& child : m_children ) {
				// Compute furthest panel point and also of it's child
				const auto panel_end = GetAbsolutePosition() + m_size;
				const auto child_end = child->GetAbsolutePosition() + child->m_size;

				// Adjust panel size if necessary
				if( ( child_end.m_y + kMargin.m_y ) >= panel_end.m_y )
					m_size.m_y = child_end.m_y - GetAbsolutePosition().m_y + kMargin.m_y;
			}
		}

		/**
		 * @brief Creates a new @ref Groupbox instance and adds it to the back of @p m_rows.
		 * @param [in] text A string, containing the name of the to-be-created @ref Groupbox instance
		 * @param [in] position The starting position of this groupbox.
		 * @param [in] size The size of this groupbox.
		 * @param [in] policy Represents the current policy of the group. See @ref GroupPolicy_t for more information.
		 * @return Pointer to the newly created @ref Groupbox.
		 */
		Groupbox* Add(
			const std::string& text,
			const Vector2D& position,
			const Vector2D& size,
			GroupPolicy_t policy
		) override {
			// Add group to the row container
			return m_children.emplace_back(
				new Groupbox( this, position + kMargin, size - kMargin, text )
			).get();
		}
	};
}

#endif // !LIBSUI_ELEMENTS_PANEL_HPP
