/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_TEXTBOX_HPP
#define LIBSUI_ELEMENTS_TEXTBOX_HPP

#pragma once

#include <utility>

#include "base_element.hpp"

namespace libsui {
	/**
	 * @brief This class represents a textbox element.
	 * @note This is a proof-of-concept SDL-specific implementation.
	 * It supports only ASCII inputs (without NUMPAD support) and doesn't handle text wrapping.
	 * @note The backspace and delete mechanics are very similar to those of CLion (and were in fact inspired by it.)
	 * The only difference is that the backspace key, when holding the CTRL modifier,
	 * will delete all special different consequtive characters such as ;'[]!. et cetera.
	 * The original CLion implementation deletes only until the occurrence of the first different special character.
	 * @note The CTRL+right arrow combination behavior is also very similar to that of CLion, except for the fact
	 * that this implementation lands on the start of the next word, and not on the end of the current one.
	 */
	class Textbox : public BaseElement {
	private:
		/**
		 * @brief The definition of the callback which is triggered when the input character is consumed.
		 */
		using OnChangeCallback = std::function< void( const std::string& input ) >;

		/**
		 * @brief A lookup table for special ASCII characters.
		 * @note Special characters in the ASCII table are in the ranges:
		 * [32, 47], [58, 64], [91, 96], [123, 126]
		 * This table starts from the 32nd characters to save a little bit of memory.
		 */
		constexpr static std::array< bool, 95 > kSpecialCharacters = {
			true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
			false, false, false, false, false, false, false, false, false, false,
			true, true, true, true, true, true, true,
			false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false, false, false,
			true, true, true, true, true, true,
			false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false, false, false,
			true, true, true, true
		};

		/**
		 * @brief A lookup table to use when holding the SHIFT modifier and typing a number.
		 */
		constexpr static std::array< char, 10 > kNumberShift{
			')', '!', '@', '#', '$', '%', '^', '&', '*', '('
		};

		/**
		 * @brief The current textbox input string.
		 */
		std::string m_input;

		/**
		 * @brief The input cursor, also know as the caret.
		 */
		std::uintptr_t m_cursor = 0;

		/**
		 * @brief Indicates whether the CTRL modifier is being held.
		 */
		bool m_holding_ctrl = false;

		/**
		 * @brief Indicates whether the LSHIFT modifier is being held.
		 */
		bool m_holding_shift = false;

		/**
		 * @brief Denotes whether the cursor should blink for the next 600ms.
		 */
		bool m_blink_enabled = false;

		/**
		 * @brief Denotes the time when the cursor last started blinking.
		 */
		std::uintptr_t m_blink_start_time = 0;

		/**
		 * @brief User-defined callback triggered when an ASCII character was consumed.
		 */
		OnChangeCallback m_on_change = nullptr;

	private:
		/**
		 * @brief Adds a character to the input buffer and moves the input cursor one position to the right.
		 * @param [in] ch Character to be added to the input buffer.
		 */
		void AddChar( char ch ) {
			m_cursor = std::min( m_cursor, m_input.size() );
			m_input.insert( m_input.begin() + static_cast< long >( m_cursor ), ch );
			++m_cursor;
		}

		/**
		 * @brief Deletes a character (or more) to the left of the cursor,
		 * based on current key modifier state.
		 * @return true, if the input buffer was modified.
		 * @return false, if the input buffer wasn't modified.
		 */
		bool OnBackspace() {
			// There's nothing to delete
			if( !m_cursor )
				return false;

			// We're not holding the CTRL modifier -- delete one character to the left of the cursor.
			if( !m_holding_ctrl ) {
				m_input.erase( --m_cursor, 1 );
			} else {
				// We're holding the CTRL modifier -- delete a word starting
				// from the current cursor position up to the word start.
				std::size_t rightmost = 0, delimiter = 0;

				// Indicates whether we're currently iterating over a word
				bool is_consequtive = false;

				// Iterate over all the characters up until the cursor
				for( auto i = 0ul; i < m_cursor; ++i ) {
					const auto ch = m_input[ i ];
					// Is this a special ASCII character?
					if( ( ch >= 32 && ch <= 126 ) && kSpecialCharacters[ ch - 32 ] ) {
						// Keep track of the rightmost special character. Reset the consequtive flag
						// used for words.
						rightmost = i;
						is_consequtive = false;
					} else {
						// Only update the delimiter if no consequtive flag is set.
						// This is so we can easily delete until word start.
						if( !is_consequtive )
							delimiter = i;

						is_consequtive = true;
					}
				}

				const auto position = std::min( delimiter, rightmost + 1 );
				m_input = m_input.substr( 0, position ) + m_input.substr( m_cursor );
				m_cursor = position;
			}

			return true;
		}

		/**
		 * @brief Deletes a character (or more) to the right of the cursor,
		 * based on current key modifier state.
		 * @return true, if the input buffer was modified.
		 * @return false, if the input buffer wasn't modified.
		 */
		bool OnDelete() {
			// There's nothing to delete
			if( m_input.empty() || m_cursor == m_input.size() )
				return false;

			// We're not holding the CTRL modifier -- delete one character to the left of the cursor.
			if( !m_holding_ctrl ) {
				m_input.erase( m_cursor, 1 );
			} else {
				// We're holding the CTRL modifier -- delete a word starting
				// from the current cursor position up to the word start/end.
				auto rightmost = std::string::npos;

				// Iterate over all the characters from the cursor until input end
				for( auto i = m_cursor + 1; i < m_input.size(); ++i ) {
					const auto ch = m_input[ i ];
					// Is this a special ASCII character?
					if( ( ch >= 32 && ch <= 126 ) && kSpecialCharacters[ ch - 32 ] ) {
						// Keep track of the rightmost special character.
						rightmost = i;
					} else {
						if( rightmost != std::string::npos )
							break;
					}
				}

				const auto position = std::min( rightmost, m_input.size() );
				m_input.erase( m_cursor, position - m_cursor );
			}

			return true;
		}

		/**
		 * @brief Moves the internal cursor to the left, based on current key modifier state.
		 */
		void OnArrowLeft() {
			if( !m_cursor )
				return;

			if( !m_holding_ctrl ) {
				--m_cursor;
			} else {
				std::size_t rightmost = 0ul;

				// Iterate over all the characters up until the cursor
				for( auto i = m_cursor - 1ul; i > 0ul; --i ) {
					const auto ch = m_input[ i ];
					// Is this a special ASCII character?
					if( ( ch >= 32 && ch <= 126 ) && kSpecialCharacters[ ch - 32 ] ) {
						// Keep track of the rightmost special character.
						rightmost = i;
						break;
					}
				}

				m_cursor = rightmost;
			}
		}

		/**
		 * @brief Moves the internal cursor to the right, based on current key modifier state.
		 */
		void OnArrowRight() {
			if( m_cursor >= m_input.size() )
				return;

			if( !m_holding_ctrl ) {
				++m_cursor;
			} else {
				auto rightmost = std::string::npos;

				// Iterate over all the characters from the cursor until input end
				for( auto i = m_cursor + 1; i < m_input.size(); ++i ) {
					const auto ch = m_input[ i ];
					// Is this a special ASCII character?
					if( ( ch >= 32 && ch <= 126 ) && kSpecialCharacters[ ch - 32 ] ) {
						// Keep track of the rightmost special character.
						rightmost = i;
					} else {
						if( rightmost != std::string::npos )
							break;
					}
				}

				m_cursor = std::min( rightmost, m_input.size() );
			}
		}

		/**
		 * @brief Handles special input keys, such as left/right arrow, space, backspace, etc.
		 * @param [in] key ASCII key code of the currently pressed key.
		 * @return true, if the @ref m_input buffer has been somehow modified (even preemptively.)
		 * @return false, if the @ref m_input buffer hasn't been modified in any way.
		 */
		bool HandleSpecialKeys( int key ) {
			if( m_input.empty() )
				return false;

			switch( key ) {
			case SDLK_LEFT:
				OnArrowLeft();
				return false;
			case SDLK_RIGHT:
				OnArrowRight();
				return false;
			case SDLK_SPACE:
				AddChar( ' ' );
				return true;
			case SDLK_BACKSPACE:
				return OnBackspace();
			case SDLK_DELETE:
				return OnDelete();
			default: return false;
			}
		}

	public:
		/**
		 * @brief Initializes this @ref Checkbox instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] size The clickable size of the current element.
		 * @param [in] placeholder The placeholder for this element.
		 * @param [in] on_change User-defined callback triggered when the input changes.
		 * @param [in] default_text The default text value for this element. This field is optional.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Textbox(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size,
			std::string placeholder = "",
			OnChangeCallback on_change = nullptr,
			std::string default_text = ""
		)
			: BaseElement( parent, position, size, std::move( placeholder ), kType_Textbox )
			, m_input( std::move( default_text ) )
			, m_cursor( m_input.size() )
			, m_on_change( std::move( on_change ) ) {}

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 */
		bool OnPress() override {
			const auto result = ( Hovered() && g_mouse->Pressing() );
			if( result ) {
				// Update cursor blinking
				m_blink_enabled = true;
				m_blink_start_time = SDL_GetTicks();
			}

			return result;
		}

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 */
		void OnRelease() override {}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 */
		void Render() override {
			const auto position = GetAbsolutePosition();
			const auto is_focused = ( m_state & kState_Focused );

			// Draw placeholder box for the current variable's name
			g_renderer->FillOutlinedRect(
				position,
				m_size,
				is_focused ? argb( 35, 35, 35 ) : argb( 45, 45, 45 ),
				Hovered() ? argb( 180, 150, 115 ) : argb( 40, 40, 40 )
			);

			g_renderer->Text(
				position.m_x + 5,
				position.m_y + 4,
				m_input.empty() ? argb( colors::Grey ) : argb( colors::White ),
				kAlign_Left,
				"%s",
				m_input.empty() ? m_text.c_str() : m_input.c_str()
			);

			if( !is_focused )
				return;

			auto width = 0, height = 0;
			g_renderer->GetTextSize( m_input.substr( 0, m_cursor ).c_str(), width, height );

			// Blink every 600ms
			if( SDL_GetTicks() > ( m_blink_start_time + 600u ) ) {
				m_blink_start_time = SDL_GetTicks();
				m_blink_enabled = !m_blink_enabled;
			}

			if( m_blink_enabled ) {
				g_renderer->Fill(
					position.m_x + width + 5,
					position.m_y + 3,
					1,
					m_size.m_y - 6,
					argb( colors::White )
				);
			}
		}

		/**
		 * @brief Modifies internal state when a key is consumed.
		 * @param [in] key A SDL key structure.
		 * @param [in] pressed Was this a key press, or a key release event?
		 */
		void OnKey( const SDL_Keysym& key, bool pressed ) {
			if( !( m_state & kState_Focused ) )
				return;

			// Update cursor blinking
			m_blink_enabled = true;
			m_blink_start_time = SDL_GetTicks();

			// Update key modifiers
			m_holding_ctrl = ( key.mod & KMOD_CTRL );
			m_holding_shift = ( key.mod & KMOD_SHIFT );

			// Don't proceed any further if no key was pressed.
			if( !pressed )
				return;

			// Handle special keys and trigger the on change callback,
			// in case the input buffer was modified
			if( HandleSpecialKeys( key.sym ) ) {
				// Trigger callback, if possible.
				if( m_on_change )
					m_on_change( m_input );

				// We've handled this case, don't proceed further.
				return;
			}

			// Allow only ASCII input
			if( key.sym > 32 && key.sym < 127 ) {
				auto ch = static_cast< char >( key.sym );
				// Convert to uppercase, in case we're holding shift
				if( m_holding_shift ) {
					if( ( ch >= 'a' && ch <= 'z' ) || ( ch >= '[' && ch <= ']' ) ) {
						ch ^= 32;
					} else if( ch >= '0' && ch <= '9' ) {
						ch = kNumberShift[ ch - '0' ];
					} else {
						switch( ch ) {
						case '`': ch = '~'; break;
						case ';': ch = ':'; break;
						case '\'': ch = '"'; break;
						case ',': ch = '<'; break;
						case '.': ch = '>'; break;
						case '/': ch = '?'; break;
						case '-': ch = '_'; break;
						case '=': ch = '+'; break;
						default: break;
						}
					}
				}

				// Update internal state and trigger on change callback
				AddChar( ch );

				// Trigger callback, if possible.
				if( m_on_change )
					m_on_change( m_input );
			}
		}

		/**
		 * @brief Overrides the internal input buffer to the given value.
		 * @param [in] new_value New value of the internal input buffer.
		 */
		inline void SetInputText( const std::string& new_value ) {
			m_input = new_value;
		}

		/**
		 * @brief Exposes the internal input buffer.
		 * @return The value of the internal input buffer.
		 */
		[[nodiscard]] inline const std::string& GetInputText() const {
			return m_input;
		}
	};

	/**
	 * @brief Specialized geometry helper for @ref Textbox elements.
	 */
	template<>
	struct ElementGeometry< Textbox > {
		/**
		 * @brief The height of a @ref Textbox element.
		 */
		constexpr static auto kHeight = 20;
	};
}

#endif // !LIBSUI_ELEMENTS_TEXTBOX_HPP
