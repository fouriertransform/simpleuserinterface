#ifndef RENDERER_HPP
#define RENDERER_HPP

#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <libsui/detail/argb.hpp>
#include <libsui/detail/renderer_interface.hpp>

using namespace libsui;

struct InternalRenderer_t : IRenderer {
	// TODO: Add assertions here
	constexpr static auto kFontPath = "/usr/share/fonts/TTF/ProggyClean.ttf";

	// NOTE: This constructor assumes that you have a valid font at the `kFontPath` location
	// Otherwise, you will crash.
	explicit InternalRenderer_t( SDL_Renderer* renderer )
		: m_font( TTF_OpenFont( kFontPath, 16 ) )
		, m_renderer( renderer ) {}

	~InternalRenderer_t() override {
		TTF_CloseFont( m_font );
	}

	void Fill(
		std::int32_t x,
		std::int32_t y,
		std::int32_t w,
		std::int32_t h,
		const argb& color
	) override {
		SDL_SetRenderDrawColor( m_renderer, color.red(), color.green(), color.blue(), color.alpha() );

		SDL_Rect rect{ x, y, w, h };
		SDL_RenderFillRect( m_renderer, &rect );
	}

	void Outline(
		std::int32_t x,
		std::int32_t y,
		std::int32_t w,
		std::int32_t h,
		const argb& color
	) override {
		SDL_SetRenderDrawColor( m_renderer, color.red(), color.green(), color.blue(), color.alpha() );

		SDL_Rect rect{ x, y, w, h };
		SDL_RenderDrawRect( m_renderer, &rect );
	}

	void FillOutlinedRect(
		std::int32_t x,
		std::int32_t y,
		std::int32_t w,
		std::int32_t h,
		const argb& color,
		const argb& outline_color
	) override {
		Fill( x + 1, y + 1, w - 1, h - 1, color );
		Outline( x, y, w, h, outline_color );
	}

	void Line(
		std::int32_t x1,
		std::int32_t y1,
		std::int32_t x2,
		std::int32_t y2,
		const argb& color
	) override {
		SDL_SetRenderDrawColor( m_renderer, color.red(), color.green(), color.blue(), color.alpha() );
		SDL_RenderDrawLine( m_renderer, x1, y1, x2, y2 );
	}

	void GetTextSize(
		const char* text,
		std::int32_t& width,
		std::int32_t& height
	) override {
		TTF_SizeText( m_font, text, &width, &height );
	}

	// Not optimal, but will suffice for a development environment
	void Text(
		std::int32_t x,
		std::int32_t y,
		const argb& color,
		std::uint8_t alignment,
		const char* text,
		...
	) override {
		// Prepare text for adjustments
		char buffer[ 512 ] = {};

		va_list args;
		va_start( args, text );
		vsnprintf( buffer, sizeof( buffer ), text, args );
		va_end( args );

		// Acquire text size, so we can apply alignment
		std::int32_t width = 0, height = 0;
		GetTextSize( buffer, width, height );

		// Horizontal adjusting
		// `kAlign_Left` is the default alignment
		if( alignment & kAlign_Right )
			x -= width;
		else if( alignment & kAlign_CenterH )
			x -= width / 2;

		// Vertical adjusting
		if( alignment & kAlign_CenterV )
			y -= height / 2;

		auto surface = TTF_RenderText_Blended(
			m_font,
			buffer,
			{ color.red(), color.green(), color.blue(), color.alpha() }
		);

		auto texture = SDL_CreateTextureFromSurface( m_renderer, surface );

		SDL_Rect rect{ x, y, width, height };
		SDL_RenderCopy( m_renderer, texture, nullptr, &rect );

		SDL_FreeSurface( surface );
		SDL_DestroyTexture( texture );
	}

	void Gradient(
		std::int32_t x,
		std::int32_t y,
		std::int32_t w,
		std::int32_t h,
		const argb& top,
		const argb& bottom
	) override {
		Fill( x, y, w, h, top );

		const auto height = static_cast< float >( h );
		for( auto i = 0; i < h; ++i ) {
			auto color = bottom;
			color.alpha( static_cast< std::uint8_t >(
				static_cast< float >( bottom.alpha() ) * ( static_cast< float >( i ) / height )
			) );
			Fill( x, y + i, w, 1, color );
		}
	}

	TTF_Font* m_font = nullptr;
	SDL_Renderer* m_renderer = nullptr;
};

struct RenderContext_t {
	~RenderContext_t() {
		if( m_renderer )
			SDL_DestroyRenderer( m_renderer );

		if( m_window )
			SDL_DestroyWindow( m_window );

		TTF_Quit();
		SDL_Quit();
	}

	bool Setup() {
		// Initialize SDL with components needed by the UI
		if( SDL_Init( SDL_INIT_TIMER | SDL_INIT_EVENTS | SDL_INIT_VIDEO ) != 0 )
			return false;

		// Create a 1280x720 window
		if( SDL_CreateWindowAndRenderer( 1280, 720, 0, &m_window, &m_renderer ) != 0 )
			return false;

		// Set the smoothest render mode
		SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "2" );
		SDL_SetRenderDrawBlendMode( m_renderer, SDL_BLENDMODE_BLEND );

		// Initialize fonts
		return !TTF_Init();
	}

	SDL_Window* m_window = nullptr;
	SDL_Renderer* m_renderer = nullptr;
};

#endif // !RENDERER_HPP
