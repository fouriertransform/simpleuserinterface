/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_BASE_ELEMENT_HPP
#define LIBSUI_ELEMENTS_BASE_ELEMENT_HPP

#pragma once

#include <string>

#include "../detail/context.hpp"
#include "../detail/reverse_iterator.hpp"

namespace libsui {
	/**
	 * @brief Represents all possible types of elements.
	 */
	enum ElementType_t : std::uint16_t {
		kType_None = 0,
		kType_Window = ( 1 << 0 ),
		kType_Tab = ( 1 << 1 ),
		kType_Panel = ( 1 << 2 ),
		kType_Groupbox = ( 1 << 3 ),
		kType_Slider = ( 1 << 4 ),
		kType_Button = ( 1 << 5 ),
		kType_Checkbox = ( 1 << 6 ),
		kType_Dropdown = ( 1 << 7 ),
		kType_Listbox = ( 1 << 8 ),
		kType_Scrollbar = ( 1 << 9 ),
		kType_Textbox = ( 1 << 10 )
	};

	/**
	 * @brief Describes in which states an element can be.
	 */
	enum ElementState_t : std::uint8_t {
		// Interaction states
		/**
		 * @brief The default element state. This means this element is either
		 * passive active or active active. Either way, it's not disabled.
		 */
		kState_Active = 0,
		/**
		 * @brief This element is disabled. No interaction
		 * should be possible when this state is set.
		 * @note Currently unused.
		 */
		kState_Disabled = ( 1 << 0 ),

		// Mouse states
		/**
		 * @brief This element is currently focused, i.e. in a special state.
		 */
		kState_Focused = ( 1 << 1 ),
		/**
		 * @brief This element (or it's parent) is being dragged.
		 */
		kState_Dragged = ( 1 << 2 ),
		/**
		 * @brief This element can be hovered, i.e. clicked and interacted with.
		 * This means that we're already hovering it's parent and there are no obstructing windows.
		 */
		kState_Interactable = ( 1 << 3 )
	};

	/**
	 * @brief Used to position components in either the leftmost part, the center
	 * or the rightmost part of the elements bounds.
	 */
	enum OffsetType_t : std::uint8_t {
		/**
		 * @brief The element should be placed on the left side of the available space.
		 */
		kOffset_Left = 2,
		/**
		 * @brief The element should be placed in the center of the available space.
		 */
		kOffset_Center,
		/**
		 * @brief The element should be placed on the right side of the available space.
		 */
		kOffset_Right
	};

	/**
	 * @brief Represents the underlying class every element must have.
	 * @note @ref m_text can be empty.
	 */
	struct BaseElement {
		/**
		 * @brief Default element constructor.
		 */
		BaseElement() = default;

		/**
		 * @brief Initializes this @ref BaseElement instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] size The clickable size of the current element.
		 * @param [in] text The text of this element. Not every element needs a @ref m_text member. This is optional.
		 * @param [in] type The type of the current element which corresponds to one of @ref ElementType_t types.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		BaseElement(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size,
			std::string text,
			const std::uint16_t type
		)
			: m_parent( parent )
			, m_anchor( parent ? parent->GetAbsolutePosition() : Vector2D{} )
			, m_position( position )
			, m_size( size )
			, m_text( std::move( text ) )
			, m_type( type ) {}

		/**
		 * @brief Pass-by-const-reference constructor overload.
		 */
		BaseElement( const BaseElement& ) = default;

		/**
		 * @brief Pass-by-const-reference assignment operator overload.
		 */
		BaseElement& operator=( const BaseElement& ) = default;

		/**
		 * @brief Rvalue constructor overload.
		 */
		BaseElement( BaseElement&& ) = default;

		/**
		 * @brief Rvalue assignment operator overload.
		 */
		BaseElement& operator=( BaseElement&& ) = default;

		/**
		 * @brief Used to deallocate child elements.
		 */
		virtual ~BaseElement() = default;

		/**
		 * @brief Adds elements to the rendering engine queue.
		 */
		virtual void Render() = 0;

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 */
		virtual bool OnPress() = 0;

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 */
		virtual void OnRelease() = 0;

		/**
		 * @brief Updates the @ref m_anchor member of the current element.
		 * @param [in] point The new @ref m_anchor point.
		 * @note Aides in updating anchors of underlying child elements.
		 * This avoids the need of having every element have a container member.
		 */
		virtual void Move( const Vector2D& point ) {
			m_anchor = point;
		}

		/**
		 * @brief Determines whether we're hovering over the current element.
		 * @return true, if mouse is hovering the element.
		 * @return false, if mouse is not hovering the element.
		 */
		virtual bool Hovered() {
			if( !CanFocus() )
				return false;

			const auto position = GetAbsolutePosition();
			return g_mouse->Over(
				position.m_x,
				position.m_y,
				m_size.m_x,
				m_size.m_y
			);
		}

		/**
		 * @brief Indicates whether this element (and potentially it's parent elements)
		 * can be interected with.
		 * @return true, if this element can be interacted with.
		 * @return false, if this element cannot be interacted with.
		 * @note Careful! You should ideally override this helper function
		 * in the hierarchically highest element in your user interface model.
		 */
		[[nodiscard]] virtual bool IsInteractable() const {
			return m_parent->IsInteractable();
		}

		/**
		 * @brief Computes the absolute point of the element's horizontal
		 * starting offset based on @ref OffsetType_t.
		 * @param [in] offset_type An offset of the current element,
		 * which determines the absolute position on the X axis.
		 * @return The absolute position on the X axis of the current element
		 * determined by the offset.
		 */
		[[nodiscard]] std::int32_t GetOffset( const OffsetType_t offset_type ) const {
			const auto offset = static_cast< float >( m_size.m_x ) * static_cast< float >( offset_type ) * 0.25f;
			return ( GetAbsolutePosition().m_x + static_cast< std::int32_t >( offset ) );
		}

		/**
		 * @brief Determines whether this element can be (re)focused.
		 * @return true, if this element can be refocused.
		 * @return false, if this element can't be refocused.
		 */
		[[nodiscard]] bool CanFocus() const {
			if( !IsInteractable() )
				return false;

			if( !g_focused_element )
				return true;

			if( g_focused_element == this )
				return true;

			return ( g_focused_element->m_type == kType_Textbox );
		}

		/**
		 * @brief Computes the absolute position of this element.
		 * @return The absolute position of this element.
		 */
		[[nodiscard]] constexpr inline Vector2D GetAbsolutePosition() const {
			return m_anchor + m_position;
		}

		/**
		 * @brief The parent element of this element.
		 * @note This will be a nullptr for the hierarchically highest element
		 * in your user interface model.
		 */
		BaseElement* m_parent = nullptr;

		/**
		 * @brief The parent anchoring position of this element.
		 * @note Sum with @ref m_position to get the absolute position of this element.
		 */
		Vector2D m_anchor;

		/**
		 * @brief The relative position of this element in regards to @ref m_anchor.
		 * @note Sum with @ref m_anchor to get the absolute position of this element.
		 */
		Vector2D m_position;

		/**
		 * @brief The renderable size of this element.
		 * @note Doesn't always correspond to the clickable area.
		 */
		Vector2D m_size;

		/**
		 * @brief Text of this element, if the element allows for it.
		 * @note This member can be empty for some elements.
		 */
		std::string m_text;

		/**
		 * @brief Type of this element.
		 */
		std::uint16_t m_type = kType_None;

		/**
		 * @brief State of this element.
		 */
		std::uint8_t m_state = kState_Active;
	};

	/**
	 * @brief Generic helper structure for determining element height based on it's type.
	 * @tparam T Type of the element.
	 */
	template< typename T >
	struct ElementGeometry {
		/**
		 * @brief The default height of any non-specialized element.
		 */
		constexpr static auto kHeight = 15;
	};
}

#endif // !LIBSUI_ELEMENTS_BASE_ELEMENT_HPP
