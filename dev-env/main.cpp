// NOTE: This is just an example development environment!
#include <iostream>
#include <vector>
#include <thread>

#include <SDL2/SDL.h>
#include <libsui/libsui.hpp>
#include <libsui/elements/textbox.sdl.hpp>

#include "sdl2_renderer.hpp"
#include "mouse.hpp"

namespace {
std::string g_last_messagebox_result;

void AddFirstTab( libsui::Tab< libsui::AutoPanel >* tab ) {
	// Setup first groupbox
	static bool checkbox_one = true;
	static bool checkbox_two = false;

	auto group_one = tab->Add( "Checkbox" );
	group_one->Add< Checkbox >( "Checked", checkbox_one );
	group_one->Add< Checkbox >( "Unchecked", checkbox_two );

	static float slider_one = 0.f;

	auto group_two = tab->Add( "Slider Float" );
	group_two->Add< Slider< float > >( "Normal", -5.f, 5.f, slider_one );
	group_two->Add< Slider< float > >( "Round", -5.f, 5.f, slider_one, kSlider_Round );

	static int slider_two = 0;

	auto group_three = tab->Add( "Slider Int" );
	group_three->Add< Slider< int > >( "Normal", -5, 5, slider_two );
	group_three->Add< Slider< int > >( "Round", -5, 5, slider_two, kSlider_Round );

	static int dropdown_one = 0;
	static int dropdown_two = 0;

	std::vector< std::string > dropdown_options{ "One", "Two", "Three" };

	auto group_four = tab->Add( "Dropdown" );
	group_four->Add< Dropdown >( "Example", dropdown_one, dropdown_options );
	group_four->Add< Dropdown >( "Empty", dropdown_two, std::vector< std::string >{} );

	auto group_five = tab->Add( "Button" );
	group_five->Add< Button >( "Test", []() {} );

	( void )tab->Add( "Empty groupbox" );

	static int listbox = 0;

	auto listbox_options = dropdown_options;
	for( auto i = 4; i < 25; ++i )
		listbox_options.emplace_back( "Option " + std::to_string( i ) );

	auto group_six = tab->Add( "Listbox", {}, {}, kPolicy_Follow | kPolicy_Fill );
	group_six->Add< Listbox >( listbox, listbox_options );

	auto group_seven = tab->Add( "Textbox" );
	group_seven->Add< Textbox >( "Placeholder only" );
	group_seven->Add< Textbox >(
		"Placeholder",
		[]( const std::string&/* value*/ ) {},
		"Initial value"
	);
}

void AddSecondTab( libsui::Menu& menu, libsui::Tab< libsui::AutoPanel >* tab ) {
	static int x = 0, y = 0;
	tab->Add( "X position" )->Add< Textbox >( "", []( const std::string& value ) {
		try {
			x = std::stoi( value );
		} catch( ... ) {}
	} );

	tab->Add( "Y position" )->Add< Textbox >( "", []( const std::string& value ) {
		try {
			y = std::stoi( value );
		} catch( ... ) {}
	} );

	auto* title_textbox = tab->Add( "Title" )->Add< Textbox >();
	auto* description_textbox = tab->Add( "Description" )->Add< Textbox >();

	static int buttons_selection = 0;
	static std::vector< std::string > buttons{ "Yes", "No" };

	auto* group_buttons = tab->Add( "Buttons", {}, {}, kPolicy_Follow | kPolicy_BreakTwice );

	auto* buttons_listbox = group_buttons->Add< Listbox >( buttons_selection, buttons );
	auto* button_name_textbox = group_buttons->Add< Textbox >();

	group_buttons->Add< Button >( "Add", [ buttons_listbox, button_name_textbox ]() {
		const auto& name = button_name_textbox->GetInputText();
		if( !name.empty() ) {
			buttons.emplace_back( name );
			buttons_listbox->Update( buttons );

			button_name_textbox->SetInputText( "" );
		}
	} );

	group_buttons->Add< Button >( "Remove", [ buttons_listbox ]() {
		if( !buttons.empty() ) {
			buttons.erase( buttons.begin() + buttons_selection );
			buttons_listbox->Update( buttons );
		}
	} );

	static int alignment_selection = 0;
	static std::vector< std::string > alignment{ "Space around", "Right" };

	auto* group_general = tab->Add( "General" );

	group_general->Add< Dropdown >( "Alignment", alignment_selection, alignment );
	group_general->Add< Button >( "Create messagebox", [ =, &menu ]() {
		std::vector< libsui::MessageboxButtons > options;
		for( const auto& button : buttons ) {
			options.emplace_back( button, [ button ]() {
				g_last_messagebox_result = button;
			} );
		}

		( void )menu.Notify(
			Vector2D{ std::max( 10, x ), std::max( 25, y ) },
			title_textbox->GetInputText(),
			{ description_textbox->GetInputText() },
			options,
			( alignment_selection == 0 ? kMessagebox_AlignSpaceAround : kMessagebox_AlignRight )
		);
	} );
}
}

std::int32_t StartRender() {
	RenderContext_t context{};
	if( !context.Setup() )
		return EXIT_FAILURE;

	InternalRenderer_t renderer( context.m_renderer );
	MouseController_t mouse{};

	libsui::Setup( &renderer, &mouse );

	// Default-construct the menu
	libsui::Menu menu{};

	// Construct a window and attach the window to the menu instance.
	// The window will:
	// - have an automatic layout positioning panel
	// - start at X = 100 and Y = 50
	// - be of size width = 620 and height = 270
	// - be captioned "Primary window"
	auto main_window = menu.Add< libsui::AutoPanel >(
		/*position = */{ 100, 50 },
		/*size = */{ 620, 270 },
		/*name = */"Primary window"
	);

	// Setup main window
	main_window->Setup(
		{ "1st tab", "2nd tab" },
		[ &menu ]( libsui::Window< libsui::AutoPanel >::Container& tabs ) {
			AddFirstTab( tabs[ 0 ].get() );
			AddSecondTab( menu, tabs[ 1 ].get() );
		}
	);

	// Apply focus to the main window
	main_window->m_state |= kState_Focused;

	bool should_exit = false;
	while( !should_exit ) {
		// Update events
		SDL_PumpEvents();
		mouse.Update();

		SDL_Event event;
		while( SDL_PollEvent( &event ) ) {
			switch( event.type ) {
			case SDL_KEYUP:
			case SDL_KEYDOWN: {
				using namespace libsui;
				if( g_focused_element && g_focused_element->m_type == libsui::kType_Textbox ) {
					auto* textbox = static_cast< Textbox* >( g_focused_element );
					textbox->OnKey( event.key.keysym, event.key.state );
				}
				break;
			}
			case SDL_QUIT:
				should_exit = true;
				break;
			case SDL_MOUSEWHEEL:
				if( event.wheel.y > 0 ) mouse.m_scroll_direction = kScroll_Up;
				else if( event.wheel.y < 0 ) mouse.m_scroll_direction = kScroll_Down;

				mouse.m_scroll_amount = -event.wheel.y;
				break;
			default:
				break;
			}
		}

		// Set background
		SDL_SetRenderDrawColor( context.m_renderer, 100, 150, 200, SDL_ALPHA_OPAQUE );
		SDL_RenderClear( context.m_renderer );

		// Handle menu input events and rendering
		menu.HandleInput();
		menu.Render();

		// Render our mouse
		mouse.Render();

		if( !g_last_messagebox_result.empty() ) {
			libsui::g_renderer->Text(
				10,
				10,
				argb( colors::White ),
				kAlign_Left,
				"%s",
				g_last_messagebox_result.c_str()
			);
		}

		// Apply drawings
		SDL_RenderPresent( context.m_renderer );

		// Invalidate scroll amount and direction for next frame
		mouse.m_scroll_amount = 0;
		mouse.m_scroll_direction = kScroll_None;

		// Wait a bit, before running again
		std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
	}

	return EXIT_SUCCESS;
}

int main( int /*argc*/, char** /*argv*/ ) {
	return StartRender();
}
