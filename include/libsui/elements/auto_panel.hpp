/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_AUTO_PANEL_HPP
#define LIBSUI_ELEMENTS_AUTO_PANEL_HPP

#pragma once

#include "base_element.hpp"
#include "base_panel.hpp"

#include <limits> // std::numeric_limits< std::int32_t >::max()

namespace libsui {
	namespace {
		/**
		 * @brief Indicates how many columns there can be in a row.
		 * @note One column can stretch the entire width of the container.
		 */
		constexpr auto kColumnsPerRow = 3;

		/**
		 * @brief Determines the minimum column span for this groupbox.
		 * @param [in] policy Policy of the groupbox currently being processed.
		 * @param [in] group_index The index of the given group in @p m_rows.
		 * @param [in] group_count The total count of groups for this row.
		 * @return Number of columns a group should take.
		 */
		constexpr int GetColumnSpan(
			GroupPolicy_t policy,
			int group_index,
			int group_count
		) {
			if( policy & kPolicy_Break )
				return group_count;
			else if( policy & kPolicy_BreakTwice )
				return 1;
			else if( policy & kPolicy_Strict )
				return kColumnsPerRow;

			const auto offset = !( policy & kPolicy_Follow ) ? -1 : 0;
			return std::max( 1, group_count + group_index + offset );
		}
	}

	/**
	 * @brief Represents a row structure in the @ref Panel interface.
	 */
	struct Row {
		/**
		 * @brief Holds all @ref Groupbox elements in this row, represented as columns.
		 */
		std::vector< std::unique_ptr< Groupbox > > m_groups;

		/**
		 * @brief Holds the policy of every group (i.e. column) in this row.
		 * @note The order of elements corresponds to the order of @ref m_groups.
		 */
		std::vector< GroupPolicy_t > m_policies;

		/**
		 * @brief Holds the height of every group (i.e. column) in this row.
		 * @note The order of elements corresponds to the order of @ref m_groups.
		 */
		std::array< std::int32_t, kColumnsPerRow > m_column_heights{};

		/**
		 * @brief Acquires the policy of the newest @ref Groupbox.
		 * @return
		 * If @ref m_groups is empty, then the @ref kPolicy_Default policy is returned.
		 * @return
		 * If @ref m_groups is not empty, then the policy of the newest @ref Groupbox is returned.
		 */
		[[nodiscard]] GroupPolicy_t GetLastPolicy() const {
			if( m_policies.empty() )
				return kPolicy_Default;
			return m_policies.back();
		}
	};

	/**
	 * @brief This class represents a panel-like element, which can hold only groupboxes.
	 * This class automatically manages the position of new groupboxes in the underlying rows based on their policies.
	 * This class also has the ability to grow itself if the accumulated height of all groupboxes exceedes the panel's height.
	 */
	class AutoPanel : public BasePanel {
	private:
		/**
		 * @brief Used to nudge element positions in @ref GetGroupWidth only by an even number or 1.
		 */
		constexpr static auto kNudgeOffset = !( kMargin.m_y % 2 ) ? ( kMargin.m_y / 2 ) : 1;

		/**
		 * @brief Container used to hold all @ref Row child elements.
		 */
		std::vector< Row > m_rows;

	private:
		/**
		 * @brief This function checks if two rectangles intersect.
		 * @param [in] position_a The starting position of the first rectangle.
		 * @param [in] size_a The size of the first rectangle.
		 * @param [in] position_b The starting position of the second rectangle.
		 * @param [in] size_b The size of the second rectangle.
		 * @return true, if the rectangles intersect.
		 * @return false, if the rectangles don't intersect.
		 */
		static bool Intersects(
			const Vector2D& position_a,
			const Vector2D& size_a,
			const Vector2D& position_b,
			const Vector2D& size_b
		) {
			return (
				( position_a.m_x < position_b.m_x + size_b.m_x ) && ( position_a.m_x + size_a.m_x > position_b.m_x ) &&
				( position_a.m_y < position_b.m_y + size_b.m_y ) && ( position_a.m_y + size_a.m_y > position_b.m_y )
			);
		}

		/**
		 * @brief Computes the width of the given @ref Groupbox element.
		 * @param [in] row_index The index in @p m_rows of the row currently being processed.
		 * @param [in] group_index The index in @p m_rows.m_groups of the group currently being processed.
		 * @return The computed width of the current @ref Groupbox instance at index @p group_index based on the specified policy.
		 * @note If the policy is not set to @ref kPolicy_Fill, then it's calculated as @ref kPolicy_Automatic.
		 */
		int GetGroupWidth(
			int row_index,
			int group_index,
			int accumulated_width
		) {
			auto& row = m_rows[ row_index ];

			// Acquire some initial info
			const auto& current_group = row.m_groups[ group_index ];
			const auto current_policy = row.m_policies[ group_index ];

			// How many groups, i.e. columns, are there in the current row?
			const auto group_count = static_cast< int >( row.m_groups.size() );

			// Adjust for strict groups policies.
			auto strict = 0;
			for( auto i = 0; i < group_index; ++i ) {
				if( row.m_policies[ i ] & kPolicy_Strict )
					++strict;
			}

			const auto column_span = (
				GetColumnSpan( current_policy, group_index, group_count ) - strict
			);

			// Compute width proportionally according to current row's group_count
			const auto surface_size = m_size.m_x + accumulated_width;

			// Don't allow custom policies if there is fewer than 2 rows.
			auto computed_width = ( surface_size - kMargin.m_x * ( column_span + 1 ) ) / column_span;
			if( row_index < 1 )
				return computed_width;

			auto vertical_nudge = 0;

			const auto current_position = current_group->GetAbsolutePosition();
			for( auto i = 0u; i < row_index; ++i ) {
				// Success checks if the new groups width would
				// intersect with any group in this row
				auto failure = false;
				for( auto j = column_span; j > 0 && !failure; --j ) {
					// Calculate the width as if the policy is @ref kPolicy_Strict
					Vector2D size( computed_width, kMargin.m_y );

					for( const auto& group : ReverseRange( m_rows[ i ].m_groups ) ) {
						auto tries = 0;
						auto intersects = true;
						while( ( intersects = Intersects(
							group->GetAbsolutePosition(),
							group->m_size + Vector2D{ 0, kMargin.m_y },
							current_position + Vector2D{ 0, vertical_nudge },
							size
						) ) ) {
							// Only groupboxes with policy set to kPolicy_Fill can have dynamic width.
							// Break here if kPolicy_Fill is not set, while making sure that the
							// intersection check is being ran regardless of current group's sizing policy
							if(
								( current_policy & kPolicy_Strict ) ||
								( current_policy & kPolicy_Follow ) ||
								!( current_policy & kPolicy_Fill )
							) {
								break;
							}

							if( ( ++tries ) > kColumnsPerRow )
								break;

							// Try resizing group if it's policy is set to kPolicy_Fill, maybe it'll fit.
							size.m_x = ( surface_size - kMargin.m_x * ( tries + 1 ) ) / tries;
						}

						if( !intersects ) {
							computed_width = size.m_x;
						} else {
							failure = true;
							break;
						}
					}
				}

				if( failure ) {
					--i;
					vertical_nudge += kNudgeOffset;
				}
			}

			// There's nothing to adjust. Stop control flow here.
			if( !vertical_nudge )
				return computed_width;

			// Adjust vertical position.
			current_group->m_position.m_y += vertical_nudge;

			// Accumulate column height and stop with control flow if last.
			const auto next_row_index = ( row_index + 1 );
			if( next_row_index >= m_rows.size() ) {
				row.m_column_heights[ group_index ] += vertical_nudge;
				return computed_width;
			}

			// Extend group height to an absurd amount and check for group intersection
			// Whichever group intersects, move it down by vertical_nudge only once
			Vector2D size(
				current_group->m_size.m_x,
				std::numeric_limits< std::int32_t >::max()
			);

			// Move groups below this row further down
			for( auto i = next_row_index; i < m_rows.size(); ++i ) {
				for( auto& other_group : m_rows[ i ].m_groups ) {
					const auto position = other_group->GetAbsolutePosition();
					if( Intersects( position, other_group->m_size, current_position, size ) )
						other_group->m_position.m_y += vertical_nudge;
				}
			}

			return computed_width;
		}

		/**
		 * @brief Computes the respective column heights of @p m_rows and
		 * repositions child elements appropriately.
		 * @note The computed element positions are not final.
		 * They can be modified later in @ref RecomputeSize during a call to @ref GetGroupWidth.
		 */
		void RecomputePosition() {
			if( m_rows.empty() )
				return;

			// Compute column heights for every row
			for( auto& row : m_rows ) {
				const auto group_count = row.m_groups.size();
				for( auto i = 0u; i < group_count; ++i ) {
					for( auto j = i; j < group_count; ++j )
						row.m_column_heights[ j ] = row.m_groups[ i ]->m_size.m_y;
				}
			}

			// Adjust positions
			for( auto i = 0; i < m_rows.size(); ++i ) {
				auto& row = m_rows[ i ];

				const auto group_count = static_cast< int >( row.m_groups.size() );
				if( !group_count )
					continue;

				auto accumulated_width = 0;
				for( auto j = 0; j < group_count; ++j ) {
					auto& group = row.m_groups[ j ];

					// Group vertical position is equal to
					// the accumulated left margin + the panel's current horizontal offset
					group->m_position.m_x = accumulated_width + ( kMargin.m_x * ( j + 1 ) );

					auto column_offset = kMargin.m_y;
					for( auto k = 0u; k < i; ++k ) {
						if( row.m_groups.size() == m_rows[ k ].m_groups.size() ) {
							column_offset += m_rows[ k ].m_column_heights[ j ];
						} else {
							const auto max_length = std::min( m_rows[ k ].m_groups.size(), row.m_groups.size() );

							auto max_height = 0;
							for( auto l = 0u; l < max_length; ++l )
								max_height = std::max( max_height, m_rows[ k ].m_column_heights[ l ] );

							column_offset += max_height;
						}
					}

					// Group vertical position is equal to
					// the accumulated top margin + the panel's current vertical offset
					group->m_position.m_y = column_offset;

					// Accumulate row width over each group
					accumulated_width += group->m_size.m_x;
				}
			}
		}

	public:
		/**
		 * @brief Initializes this @ref Panel instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position A point which is relative to @ref m_anchor.
		 * @param [in] size The size of the current element.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		AutoPanel(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size
		)
			: BasePanel( parent, position, size ) {}

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 * @note This function invokes underlying child callbacks.
		 */
		bool OnPress() override {
			for( auto& row : m_rows ) {
				for( auto& group : row.m_groups ) {
					if( group->OnPress() )
						return true;
				}
			}

			return ( Hovered() && g_mouse->Pressing() );
		}

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 * @note This function invokes underlying child callbacks.
		 */
		void OnRelease() override {
			for( auto& row : m_rows ) {
				for( auto& group : row.m_groups )
					group->OnRelease();
			}
		}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 * @note This function renders the underlying @ref Groupbox elements.
		 */
		void Render() override {
			// Draw window
			g_renderer->FillOutlinedRect(
				GetAbsolutePosition(),
				m_size,
				argb( 65, 65, 65 ),
				argb( 40, 40, 40 )
			);

			// Draw children elements in reverse order,
			// so we can render dropdowns properly
			for( auto& row : ReverseRange( m_rows ) ) {
				for( auto& group : row.m_groups )
					group->Render();
			}
		}

		/**
		 * @brief Updates the @ref m_anchor member of the current element.
		 * @param [in] point The new @ref m_anchor point.
		 * @note This function moves the underlying child elements.
		 */
		void Move( const Vector2D& point ) override {
			// Update current element's anchor
			BaseElement::Move( point );

			// Move all elements
			for( auto& row : m_rows ) {
				for( auto& group : row.m_groups )
					group->Move( m_position + point );
			}
		}

		/**
		 * @brief Recomputes the size and position of the current element (and it's children)
		 * and adjusts the height(s), if necessary.
		 */
		void RecomputeGeometry() override {
			if( m_rows.empty() )
				return;

			// Recompute child positions
			RecomputePosition();

			// Iterate over all rows in the panel
			for( auto i = 0; i < m_rows.size(); ++i ) {
				const auto& row = m_rows[ i ];

				const auto group_count = static_cast< int >( row.m_groups.size() );
				if( !group_count )
					continue;

				// Used as sort of a horizontal offset
				auto accumulated_width = 0;
				for( auto j = 0; j < group_count; ++j ) {
					auto& group = row.m_groups[ j ];

					// Adjust downsizing by moving back
					group->m_position.m_x = accumulated_width + ( kMargin.m_x * ( j + 1 ) );

					// Update group size
					const auto new_size = GetGroupWidth( i, j, accumulated_width );
					group->m_size.m_x = new_size;

					// Accumulate horizontal offset
					accumulated_width += group->m_size.m_x;

					// Resize child elements
					const auto element_width = new_size - ( kMargin.m_x * 4 );
					for( auto& child : group->m_children )
						child->m_size.m_x = element_width;

					// Update group child element anchors
					group->Move( group->m_anchor );
				}
			}

			// Adjust panel size if necessary
			for( const auto& row : m_rows ) {
				for( const auto& child : row.m_groups ) {
					// Compute furthest panel point and also of it's child
					const auto panel_end = GetAbsolutePosition() + m_size;
					const auto child_end = child->GetAbsolutePosition() + child->m_size;

					// Check whether the child element overflows its parent
					// and update height if it does.
					if( ( child_end.m_y + kMargin.m_y ) >= panel_end.m_y )
						m_size.m_y = child_end.m_y - GetAbsolutePosition().m_y + kMargin.m_y;
				}
			}
		}

		/**
		 * @brief Creates a new @ref Groupbox instance and adds it to the back of @p m_rows.
		 * @param [in] text A string, containing the name of the to-be-created @ref Groupbox instance
		 * @param [in] position The starting position of this groupbox. Unused.
		 * @param [in] size The size of this groupbox. Unused.
		 * @param [in] policy Represents the current policy of the group. See @ref GroupPolicy_t for more information.
		 * @return Pointer to the newly created @ref Groupbox.
		 */
		Groupbox* Add(
			const std::string& text,
			const Vector2D& position,
			const Vector2D& size,
			GroupPolicy_t policy
		) override {
			// Add first row
			if( m_rows.empty() )
				( void )m_rows.emplace_back();

			// Add a new row if either:
			// - the last row's last policy was @ref kPolicy_BreakTwice
			// - after every @ref kGroupboxesPerRow -th groupbox
			// - the current policy is @ref kPolicy_Break or @ref kPolicy_BreakTwice
			// and we have groupboxes in the current row
			const auto is_break_policy = ( policy & kPolicy_Break || policy & kPolicy_BreakTwice );
			if( ( m_rows.back().GetLastPolicy() & kPolicy_BreakTwice ) ||
				( m_rows.back().m_groups.size() >= kColumnsPerRow ) ||
				( !m_rows.back().m_groups.empty() && is_break_policy ) )
				( void )m_rows.emplace_back();

			// Add policy to the row container
			( void )m_rows.back().m_policies.emplace_back( policy );

			// Add group to the row container and return its pointer.
			return m_rows.back().m_groups.emplace_back(
				new Groupbox( this, {}, kMargin, text )
			).get();
		}
	};
}

#endif // !LIBSUI_ELEMENTS_AUTO_PANEL_HPP
