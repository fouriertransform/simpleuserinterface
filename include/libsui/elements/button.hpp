/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_BUTTON_HPP
#define LIBSUI_ELEMENTS_BUTTON_HPP

#pragma once

#include "base_element.hpp"

#include <functional> // std::function
#include <utility>

namespace libsui {
	/**
	 * @brief This class represents a button element that provides an on-click callback.
	 */
	class Button : public BaseElement {
	private:
		/**
		 * @brief Callback to be fired on button press release.
		 */
		std::function< void() > m_callback;

	public:
		/**
		 * @brief Initializes this @ref Button instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] size The clickable size of the current element.
		 * @param [in] text The text of this element. Not every element needs a @ref m_text member. This is optional.
		 * @param [in] callback Callback that should be invoked when the @ref OnPress is invoked.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Button(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size,
			const std::string& text,
			std::function< void() > callback
		)
			: BaseElement( parent, position, size, text, kType_Button )
			, m_callback( std::move( callback ) ) {}

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 */
		bool OnPress() override {
			return Hovered();
		}

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 */
		void OnRelease() override {
			if( m_callback && Hovered() )
				m_callback();
		}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 */
		void Render() override {
			const auto pressed = (
				g_mouse->Pressing() && ( ( m_state & kState_Focused ) || Hovered() )
			);

			const auto position = GetAbsolutePosition();

			// Draw current variable name placeholder box
			g_renderer->Gradient(
				position + 1,
				m_size - 2,
				!pressed ? argb( 94, 99, 96 ) : argb( 31, 32, 32 ),
				!pressed ? argb( 58, 60, 59 ) : argb( 54, 57, 55 )
			);

			g_renderer->Outline( position, m_size, argb( 25, 27, 26 ) );

			if( !pressed ) {
				g_renderer->Line(
					position + 1,
					position + Vector2D{ m_size.m_x - 2, 1 },
					argb( 125, 130, 127 )
				);

				g_renderer->Line(
					position + Vector2D{ 0, m_size.m_y - 1 },
					position + m_size - 1,
					argb( 5, 5, 5 )
				);

				if( Hovered() )
					g_renderer->Outline( position - 1, m_size + 2, argb( colors::White, 65 ) );
			} else {
				g_renderer->Line(
					position,
					position + Vector2D{ m_size.m_x - 1, 0 },
					argb( 13, 13, 13 )
				);
			}

			// Draw @ref m_text
			g_renderer->Text(
				position.m_x + m_size.m_x / 2,
				position.m_y + m_size.m_y / 2,
				argb( colors::White ),
				kAlign_CenterH | kAlign_CenterV,
				"%s",
				m_text.c_str()
			);
		}
	};

	/**
	 * @brief Specialized geometry helper for @ref Button elements.
	 */
	template<>
	struct ElementGeometry< Button > {
		/**
		 * @brief The height of a @ref Button element.
		 */
		constexpr static auto kHeight = 20;
	};
}

#endif // !LIBSUI_ELEMENTS_BUTTON_HPP
