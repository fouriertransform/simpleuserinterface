/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_DETAIL_REVERSE_ITERATOR_HPP
#define LIBSUI_DETAIL_REVERSE_ITERATOR_HPP

#pragma once

#include <iterator> // std::make_reverse_iterator

namespace libsui {
	/**
	 * @brief This class is used to reverse a range from a given std container.
	 */
	template< class T >
	class ReverseRange {
	private:
		const T& m_range;

	public:
		constexpr explicit ReverseRange( const T& range ) noexcept
			: m_range( range ) {}

		constexpr auto begin() const noexcept { return std::make_reverse_iterator( std::end( m_range ) ); }
		constexpr auto end() const noexcept { return std::make_reverse_iterator( std::begin( m_range  ) ); }
	};
}

#endif // !LIBSUI_DETAIL_REVERSE_ITERATOR_HPP
