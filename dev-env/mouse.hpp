#ifndef MOUSE_HPP
#define MOUSE_HPP

#pragma once

#include "sdl2_renderer.hpp"

#include <libsui/detail/context.hpp>
#include <libsui/detail/argb.hpp>

using namespace libsui;

struct MouseController_t : IMouse {
	MouseController_t() = default;

	void Render() override {
		g_renderer->Fill( m_position.m_x, m_position.m_y, 1, 12, argb( colors::White ) );
		g_renderer->Fill( m_position.m_x + 1, m_position.m_y + 0, 1, 11, argb( colors::White ) );
		g_renderer->Fill( m_position.m_x + 2, m_position.m_y + 1, 1, 1, argb( colors::White ) );
		g_renderer->Fill( m_position.m_x + 3, m_position.m_y + 2, 1, 1, argb( colors::White ) );
		g_renderer->Fill( m_position.m_x + 4, m_position.m_y + 3, 1, 1, argb( colors::White ) );
		g_renderer->Fill( m_position.m_x + 5, m_position.m_y + 4, 1, 1, argb( colors::White ) );
		g_renderer->Fill( m_position.m_x + 6, m_position.m_y + 5, 1, 1, argb( colors::White ) );
		g_renderer->Fill( m_position.m_x + 7, m_position.m_y + 6, 1, 1, argb( colors::White ) );
		g_renderer->Fill( m_position.m_x + 8, m_position.m_y + 7, 1, 1, argb( colors::White ) );

		g_renderer->Fill( m_position.m_x + 4, m_position.m_y + 8, 4, 1, argb( colors::White ) );
		g_renderer->Fill( m_position.m_x + 3, m_position.m_y + 9, 1, 1, argb( colors::White ) );
		g_renderer->Fill( m_position.m_x + 2, m_position.m_y + 10, 1, 1, argb( colors::White ) );
		g_renderer->Fill( m_position.m_x + 1, m_position.m_y + 11, 1, 1, argb( colors::White ) );

		g_renderer->Fill( m_position.m_x + 1, m_position.m_y + 1, 1, 10, argb( colors::Black ) );
		g_renderer->Fill( m_position.m_x + 2, m_position.m_y + 2, 1, 8, argb( colors::Black ) );
		g_renderer->Fill( m_position.m_x + 3, m_position.m_y + 3, 1, 6, argb( colors::Black ) );
		g_renderer->Fill( m_position.m_x + 4, m_position.m_y + 4, 1, 4, argb( colors::Black ) );
		g_renderer->Fill( m_position.m_x + 5, m_position.m_y + 5, 1, 3, argb( colors::Black ) );
		g_renderer->Fill( m_position.m_x + 6, m_position.m_y + 6, 1, 2, argb( colors::Black ) );
		g_renderer->Fill( m_position.m_x + 7, m_position.m_y + 7, 1, 1, argb( colors::Black ) );
	}

	bool Pressing() override {
		return static_cast< bool >( m_state & SDL_BUTTON( SDL_BUTTON_LEFT ) );
	}

	bool Pressed() override {
		static std::uintptr_t last_time = 0;

		if( Pressing() && ( SDL_GetTicks() > ( last_time + 250u ) ) ) {
			last_time = SDL_GetTicks();
			return true;
		}

		return false;
	}

	bool Over(
		std::int32_t x,
		std::int32_t y,
		std::int32_t w,
		std::int32_t h
	) override {
		return (
			m_position.m_x >= x && m_position.m_x < ( x + w ) &&
			m_position.m_y >= y && m_position.m_y < ( y + h )
		);
	}

	void Update() {
		m_state = SDL_GetMouseState( &m_position.m_x, &m_position.m_y );
	}

	std::uintptr_t m_state = 0;
};

inline MouseController_t g_mouse{};

#endif // !MOUSE_HPP
