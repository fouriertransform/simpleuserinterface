/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_BASE_PANEL_HPP
#define LIBSUI_ELEMENTS_BASE_PANEL_HPP

#pragma once

#include "base_element.hpp"
#include "groupbox.hpp"

#include "../detail/scaled_cutter.hpp"
#include "../detail/relative_cutter.hpp"

namespace libsui {
	/**
	 * @brief Represents how this groupbox should be aligned in the row-centric grid.
	 * @note To avoid undefined behavior, you should combine one positioning policy
	 * with one sizing policy, or simply use a predefined policy.
	 */
	enum GroupPolicy_t : std::uint8_t {
		// Empty policy
		kPolicy_None = 0,

		// Positioning policies
		/**
		 * @brief Indicates that this @ref Groupbox instance should be
		 * appended to the newest row, or if not possible, in the next row.
		 */
		kPolicy_Append = ( 1 << 0 ),
		/**
		 * @brief Indicates that this @ref Groupbox instance should be
		 * added in a separate row.
		 */
		kPolicy_Break = ( 1 << 1 ),
		/**
		 * @brief Indicates that this @ref Groupbox instance should be
		 * added in a separate row, followed by another new row.
		 * This policy guarantees that this @ref Groupbox instance will use
		 * the entire available parent width while ensuring no interesction
		 * with another @ref Groupbox element occurs.
		 */
		kPolicy_BreakTwice = ( 1 << 2 ),
		/**
		 * @brief Indicates that this @ref Groupbox instance should be
		 * added in the current row, just below the group with the same group index
		 * from the previous row. If there are no groups with the corresponding index
		 * then the group will be added as if @ref kPolicy_Append was specified.
		 */
		kPolicy_Follow = ( 1 << 3 ),

		// Sizing policies
		/**
		 * @brief Indicates that this @ref Groupbox will fill empty space
		 * until the group encounters an intersection from another group from previous rows.
		 */
		kPolicy_Fill = ( 1 << 4 ),
		/**
		 * @brief Indicates that this @ref Groupbox will fill empty space
		 * equal to the default groupbox size, which is equal to the groups width
		 * divided by kGroupboxesPerRow, without checking for intersections with previous rows.
		 */
		kPolicy_Strict = ( 1 << 5 ),

		// Predefined policies
		/**
		 * @brief The default policy is defined as a combination of
		 * @ref kPolicy_Fill and @ref kPolicy_Follow, which allows for smooth stacking
		 * of groups underneath each other.
		 */
		kPolicy_Default = ( kPolicy_Follow | kPolicy_Fill )
	};

	/**
	 * @brief Overload for the bitwise inclusive OR operator.
	 * @param [in] a A policy from the @p GroupPolicy_t enum.
	 * @param [in] b Another policy from the @p GroupPolicy_t enum.
	 * @return Bitwise inclusive a OR b
	 */
	constexpr GroupPolicy_t operator|( const GroupPolicy_t a, const GroupPolicy_t b ) {
		return static_cast< GroupPolicy_t >( static_cast< int >( a ) | static_cast< int >( b ) );
	}

	/**
	 * @brief This class represents a panel-like element, which can be "cut" by either using
	 * @ref ScaledCutter of @ref RelativeCutter.
	 * This class also has the ability to increase it's height if
	 * the furthest point of a child element exceeds the panel's furthest point.
	 */
	class BasePanel : public BaseElement {
	private:
	protected:
		/**
		 * @brief Used for calculating margins inside the panel.
		 */
		constexpr static Vector2D kMargin{ 6, 12 };

	public:
		/**
		 * @brief Initializes this @ref Panel instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position A point which is relative to @ref m_anchor.
		 * @param [in] size The size of the current element.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		BasePanel(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size
		)
			: BaseElement( parent, position, size, "", kType_Panel ) {}

		/**
		 * @brief Recomputes the size of the current element and adjusts the height, if necessary.
		 */
		virtual void RecomputeGeometry() = 0;

		/**
		 * @brief Creates a new @ref Groupbox instance and adds it to the back of @p m_rows.
		 * @param [in] text A string, containing the name of the to-be-created @ref Groupbox instance
		 * @param [in] position The starting position of this groupbox.
		 * @param [in] size The size of this groupbox.
		 * @param [in] policy Represents the current policy of the group. See @ref GroupPolicy_t for more information.
		 * @return Pointer to the newly created @ref Groupbox.
		 */
		virtual Groupbox* Add(
			const std::string& text,
			const Vector2D& position,
			const Vector2D& size,
			GroupPolicy_t policy
		) = 0;

		/**
		 * @brief Constructs a @ref ScaledCutter element which can futher be "cut" into scaled parts.
		 * @return A new ScaledCutter instance.
		 */
		[[nodiscard]] inline ScaledCutter Scaled() const {
			return ScaledCutter( m_position + kMargin, m_position + m_size );
		}

		/**
		 * @brief Constructs a @ref RelativeCutter element which can futher be "cut" into relative (px) parts.
		 * @return A new RelativeCutter instance.
		 */
		[[nodiscard]] inline RelativeCutter Relative() const {
			return RelativeCutter( m_position + kMargin, m_position + m_size );
		}
	};
}

#endif // !LIBSUI_ELEMENTS_BASE_PANEL_HPP
