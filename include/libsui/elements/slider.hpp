/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_SLIDER_HPP
#define LIBSUI_ELEMENTS_SLIDER_HPP

#pragma once

#include "base_element.hpp"
#include "button.hpp"

#include <algorithm>
#include <cmath>

namespace libsui {
	/**
	 * @brief Describes how the slider should behave with values.
	 */
	enum SliderMode_t : std::uint8_t {
		/**
		 * @brief Means that the slider should compute the full value,
		 * but use only the first 2 digits for rendering.
		 */
		kSlider_Normal,
		/**
		 * @brief Means that the slider should round the computed value
		 * to the nearest integer and render the value as an integer.
		 */
		kSlider_Round
	};

	/**
	 * @brief This class represents a horizontal slider.
	 */
	template< typename T >
	class Slider : public BaseElement {
	private:
		/**
		 * @brief The maximum tick frequency to be displayed, if possible.
		 */
		constexpr static auto kTickFrequency = 10;

		/**
		 * @brief Current slider value calculation mode.
		 */
		SliderMode_t m_mode = kSlider_Normal;

		/**
		 * @brief The minimal allowed value for @p m_variable.
		 */
		T m_min;

		/**
		 * @brief The maximal allowed value for @p m_variable.
		 */
		T m_max;

		/**
		 * @brief Variable to be used for interacting with this element.
		 */
		T& m_variable;

		/**
		 * @brief Value formatter to be used for displaying the value.
		 */
		std::string m_formatter;

		/**
		 * @brief Number of slider ticks.
		 */
		int m_ticks = 0;

	private:
		/**
		 * @brief Acquires the current value as a percentage.
		 * @return Percentage of the current value in the [m_min, m_max] range.
		 */
		[[nodiscard]] inline float GetPercentage() const {
			return ( static_cast< float >( m_variable - m_min ) / ( m_max - m_min ) );
		}

		/**
		 * @brief Helper to determine whether this class instance contains a floating point variable.
		 */
		inline constexpr static const char* GetFormatter( SliderMode_t mode ) {
			return (
				std::is_same< T, float >::value ?
				( ( mode == kSlider_Normal ) ? "%.2f" : "%.0f" ) :
				"%d"
			);
		}

		/**
		 * @brief Computes the number of ticks below the slider.
		 * @param [in] min The minimum value for this instance.
		 * @param [in] max The maximum value for this instance.
		 * @return The number of slider ticks.
		 */
		inline constexpr static int GetNumberOfTicks( T min, T max ) {
			// Draw ticks underneath the slider bar
			const auto ticks = (
				( max >= kTickFrequency ) ?
				static_cast< int >( ( ( max + min ) * kTickFrequency ) / ( max - min ) ) :
				static_cast< int >( max )
			);

			// Adjust for sliders with a small scale.
			// But don't adjust for sliders that have kSlider_Round as their mode.
			if( ( ( max - min ) < kTickFrequency ) ) {
				if constexpr( std::is_same< T, float >::value )
					return kTickFrequency;
				return ticks - 1;
			}

			return ticks;
		}

	public:
		/**
		 * @brief Initializes this @ref Slider instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] size The clickable size of the current element.
		 * @param [in] text The text of this element. Not every element needs a @ref m_text member. This is optional.
		 * @param [in] min Minimum value of the @p m_variable member.
		 * @param [in] max Maximum value of the @p m_variable member.
		 * @param [in,out] variable The variable which should be modified on value change.
		 * @param [in] mode The mode which dictates how the @p m_variable value should be handled.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Slider(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size,
			const std::string& text,
			T min,
			T max,
			T& variable,
			SliderMode_t mode = kSlider_Normal
		)
			: BaseElement( parent, position, size, text, kType_Slider )
			, m_mode( mode )
			, m_min( min )
			, m_max( max )
			, m_variable( variable )
			, m_formatter( GetFormatter( mode ) )
			, m_ticks( GetNumberOfTicks( min, max ) ) {}

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 * @note Input is always consumed inside the @ref Slider class.
		 */
		bool OnPress() override {
			// How much pixels have we dragged?
			const auto surface = static_cast< float >(
				g_mouse->m_position.m_x - GetAbsolutePosition().m_x
			);

			// How much do we have to scale our variable in the [0, 1] range?
			const auto ratio = (
				surface / static_cast< float >( m_size.m_x )
			);

			// Compute the value of the current variable in the [m_min, m_max] range.
			m_variable = std::clamp(
				static_cast< T >( m_min + ( m_max - m_min ) * ratio ),
				m_min,
				m_max
			);

			// Round the value, if necessary.
			if( m_mode == kSlider_Round )
				m_variable = std::ceil( m_variable );

			// std::ceil copies the sign, so -0.001 becomes -0
			// and since this is ugly to display, we simply convert it to a positive 0
			if( m_variable == -0 )
				m_variable = 0;

			return true;
		}

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 */
		void OnRelease() override {}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 */
		void Render() override {
			// Position of the slider thumb on the surface, based on the current value
			auto thumb_position = static_cast< int >( m_size.m_x * GetPercentage() );

			// Is the slider in some way focused?
			const auto has_focus = ( Hovered() || ( m_state & kState_Focused ) );

			// Get absolute position of this element
			const auto position = GetAbsolutePosition();

			// The ratio by how much every pixel is distant to another
			const auto pixels_per_tick = m_size.m_x / std::max( m_ticks, 1 );

			// Draw slider tick underneath the slider bar
			for( auto i = 0u; i < m_ticks + 1; ++i ) {
				// Should this tick be larger than others?
				const auto is_special = (
					( i == 0 ) ||
					( i == static_cast< int >( m_ticks / 2 ) ) ||
					( i == m_ticks )
				);

				// Calculate the position of the current tick
				const auto tick_position = static_cast< int >( i * pixels_per_tick );

				// Draw current tick
				g_renderer->Fill(
					position.m_x + tick_position,
					position.m_y + 20 + 1,
					1,
					is_special ? 7 : 5,
					( has_focus && thumb_position >= tick_position ) ?
					argb( 180, 150, 115 ) :
					argb( 40, 40, 40 )
				);
			}

			// Draw background bar outline
			g_renderer->Gradient(
				position.m_x,
				position.m_y + 15 + 1,
				m_size.m_x + 1,
				6,
				!has_focus ? argb( 40, 40, 40 ) : argb( 180, 150, 115 ),
				!has_focus ? argb( 30, 30, 30 ) : argb( 110, 80, 40 )
			);

			// Draw background bar
			g_renderer->Gradient(
				position.m_x + 1,
				position.m_y + 15 + 2,
				m_size.m_x - 1,
				4,
				argb( 45, 45, 45 ),
				argb( 30, 30, 30 )
			);

			// Hacky fix to make sure we're not going to draw over
			// the slider border when the current value is equal to m_max
			if( thumb_position == m_size.m_x )
				--thumb_position;

			// Fill background bar up to the current thumb position
			g_renderer->Gradient(
				position.m_x + 1,
				position.m_y + 15 + 2,
				thumb_position,
				4,
				argb( 180, 150, 115 ),
				argb( 110, 80, 40 )
			);

			// Draw variable value
			g_renderer->Text(
				position.m_x + m_size.m_x,
				position.m_y + 5 + 1,
				argb( colors::White ),
				kAlign_Right | kAlign_CenterV,
				m_formatter.c_str(),
				m_variable
			);

			// Draw @ref m_text
			g_renderer->Text(
				position.m_x,
				position.m_y + 5 + 1,
				argb( colors::White ),
				kAlign_Left | kAlign_CenterV,
				"%s",
				m_text.c_str()
			);
		}

		/**
		 * @brief Determines whether we're hovering over the current element.
		 * @return true, if mouse is hovering the element.
		 * @return false, if mouse is not hovering the element.
		 */
		bool Hovered() override {
			if( !CanFocus() )
				return false;

			const auto position = GetAbsolutePosition();
			return g_mouse->Over(
				position.m_x - 1,
				position.m_y + 15,
				m_size.m_x + 4,
				10
			);
		}
	};

	/**
	 * @brief Specialized geometry helper for @ref Slider elements.
	 */
	template< typename U >
	struct ElementGeometry< Slider< U > > {
		/**
		 * @brief The height of a @ref Slider element.
		 */
		constexpr static auto kHeight = 27;
	};
}

#endif // !LIBSUI_ELEMENTS_SLIDER_HPP
