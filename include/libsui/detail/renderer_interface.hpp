/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_DETAIL_RENDERER_INTERFACE_HPP
#define LIBSUI_DETAIL_RENDERER_INTERFACE_HPP

#pragma once

#include <cstdint> // std::int32_t

#include "argb.hpp" // argb
#include "vector2d.hpp" // Vector2D

namespace libsui {
	/**
	 * @brief Describes how text can be aligned, relative to it's position and size.
	 */
	enum FontAlignment_t : std::uint8_t {
		/**
		 * @brief This text should be centered vertically.
		 */
		kAlign_CenterV = ( 1 << 0 ),
		/**
		 * @brief This text should be centered vertically.
		 */
		kAlign_CenterH = ( 1 << 2 ),
		/**
		 * @brief This text should be justified to the left.
		 */
		kAlign_Left = ( 1 << 3 ),
		/**
		 * @brief This text should be justified to the right.
		 */
		kAlign_Right = ( 1 << 4 )
	};

	/**
	 * @brief Interface for implementing a rendering engine wrapper.
	 */
	struct IRenderer {
		/**
		 * @brief Default virtual destructor, which can be overloaded to aid
		 * in safely calling delete on this object in case of dynamic allocation.
		 */
		virtual ~IRenderer() = default;

		/**
		 * @brief Should fill the given area with the given color.
		 * @param [in] x Starting point on the X axis.
		 * @param [in] y Starting point on the Y axis.
		 * @param [in] w Width of the area.
		 * @param [in] h Height of the area
		 * @param [in] color Color of the area.
		 */
		virtual void Fill(
			std::int32_t x,
			std::int32_t y,
			std::int32_t w,
			std::int32_t h,
			const argb& color
		) = 0;

		/**
		 * @brief Helper overload for it's virtual function counterpart.
		 * @param [in] position The starting point.
		 * @param [in] size The size of the area.
		 * @param [in] color The color of the fill.
		 */
		inline void Fill( const Vector2D& position, const Vector2D& size, const argb& color ) {
			Fill( position.m_x, position.m_y, size.m_x, size.m_y, color );
		}

		/**
		 * @brief Should draw an outline of the given area with the given color.
		 * @param [in] x Starting point on the X axis.
		 * @param [in] y Starting point on the Y axis.
		 * @param [in] w Width of the area.
		 * @param [in] h Height of the area
		 * @param [in] color Color of the area.
		 */
		virtual void Outline(
			std::int32_t x,
			std::int32_t y,
			std::int32_t w,
			std::int32_t h,
			const argb& color
		) = 0;

		/**
		 * @brief Helper overload for it's virtual function counterpart.
		 * @param [in] position The starting point.
		 * @param [in] size The size of the area.
		 * @param [in] color The color of the fill.
		 */
		inline void Outline( const Vector2D& position, const Vector2D& size, const argb& color ) {
			Outline( position.m_x, position.m_y, size.m_x, size.m_y, color );
		}

		/**
		 * @brief Fills the given area with the given color, and draws an outline with the other given color.
		 * @param [in] x Starting point on the X axis.
		 * @param [in] y Starting point on the Y axis.
		 * @param [in] w Width of the area.
		 * @param [in] h Height of the area
		 * @param [in] color Color of the area.
		 * @param [in] outline_color Color of the outline.
		 */
		virtual void FillOutlinedRect(
			std::int32_t x,
			std::int32_t y,
			std::int32_t w,
			std::int32_t h,
			const argb& color,
			const argb& outline_color
		) = 0;

		/**
		 * @brief Helper overload for it's virtual function counterpart.
		 * @param [in] position The starting point.
		 * @param [in] size The size of the area.
		 * @param [in] color The color of the fill.
		 * @param [in] outline_color The color of the outer (border) line.
		 */
		inline void FillOutlinedRect(
			const Vector2D& position,
			const Vector2D& size,
			const argb& color,
			const argb& outline_color
		) {
			FillOutlinedRect( position.m_x, position.m_y, size.m_x, size.m_y, color, outline_color );
		}

		/**
		 * @brief Draws a outline of the given area.
		 * @param [in] x1 Starting point on the X axis.
		 * @param [in] y1 Starting point on the Y axis.
		 * @param [in] x2 Ending point on the X axis.
		 * @param [in] y2 Ending point on the Y axis.
		 * @param [in] color Color of the area.
		 */
		virtual void Line(
			std::int32_t x1,
			std::int32_t y1,
			std::int32_t x2,
			std::int32_t y2,
			const argb& color
		) = 0;

		/**
		 * @brief Helper overload for it's virtual function counterpart.
		 * @param [in] start_point The starting point.
		 * @param [in] end_point The ending point.
		 * @param [in] color The color of the fill.
		 */
		inline void Line(
			const Vector2D& start_point,
			const Vector2D& end_point,
			const argb& color
		) {
			Line( start_point.m_x, start_point.m_y, end_point.m_x, end_point.m_y, color );
		}

		/**
		 * @brief Should determine the current text string's width and height in pixels.
		 * @param [in] text The text string for which the width and height should be determined.
		 * @param [out] width The width of the text string, in pixels.
		 * @param [out] height The height of the text string, in pixels
		 */
		virtual void GetTextSize(
			const char* text,
			std::int32_t& width,
			std::int32_t& height
		) = 0;

		/**
		 * @brief Should draw a text string based on the current text alignment and color.
		 * @param [in] x Starting position of the text on the X axis.
		 * @param [in] y Starting position of the text on the Y axis.
		 * @param [in] color Color of the text.
		 * @param [in] alignment Alignent of the text, relative to it's position and size.
		 * @param [in] text The text that should be drawn.
		 */
		virtual void Text(
			std::int32_t x,
			std::int32_t y,
			const argb& color,
			std::uint8_t alignment,
			const char* text,
			...
		) = 0;

		/**
		 * @brief Should draw a vertical gradient in the specified area with a smooth color transition from top to bottom.
		 * @param [in] x Starting point on the X axis.
		 * @param [in] y Starting point on the Y axis.
		 * @param [in] w Width of the area.
		 * @param [in] h Height of the area
		 * @param [in] top Color of the top part of the gradient.
		 * @param [in] bottom Color of the bottom part of the gradient.
		 */
		virtual void Gradient(
			std::int32_t x,
			std::int32_t y,
			std::int32_t w,
			std::int32_t h,
			const argb& top,
			const argb& bottom
		) = 0;

		/**
		 * @brief Helper overload for it's virtual function counterpart.
		 * @param [in] position The starting point.
		 * @param [in] size The size of the area.
		 * @param [in] top Color of the top part of the gradient.
		 * @param [in] bottom Color of the bottom part of the gradient.
		 */
		inline void Gradient(
			const Vector2D& position,
			const Vector2D& size,
			const argb& top,
			const argb& bottom
		) {
			Gradient( position.m_x, position.m_y, size.m_x, size.m_y, top, bottom );
		}
	};
}

#endif // !LIBSUI_DETAIL_RENDERER_INTERFACE_HPP
