/*
 * SimpleUserInterface - An easy-to-use, intuitive C++17 library for straightforward UIs.
 * Copyright (C) 2019-2021  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of SimpleUserInterface.
 *
 * SimpleUserInterface is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SimpleUserInterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SimpleUserInterface.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBSUI_ELEMENTS_GROUPBOX_HPP
#define LIBSUI_ELEMENTS_GROUPBOX_HPP

#pragma once

#include <vector> // std::vector
#include <memory> // std::unique_ptr

#include "base_element.hpp"

namespace libsui {
	/**
	 * @brief This class represents a groupbox element.
	 * This class also handles focusing events for it's child elements.
	 */
	class Groupbox : public BaseElement {
	public:
		/**
		 * @brief Used for calculating margins inside the groupbox.
		 */
		constexpr static Vector2D kMargin{ 12, 12 };

		/**
		 * @brief Container to hold all child elements.
		 */
		std::vector< std::unique_ptr< BaseElement > > m_children;

		/**
		 * @brief The accumulated height of all the elements inside the groupbox.
		 * Doesn't always correspond to the horizontal property of @p m_size.
		 */
		int m_height = kMargin.m_y;

		/**
		 * @brief Width of the @ref m_text string in pixels.
		 * @note Calculated at instantiation.
		 */
		int m_text_width = 0;

	public:
		/**
		 * @brief Initializes this @ref Groupbox instance.
		 * @param [in] parent Points to the parent element of this element.
		 * @param [in] position Position of the current element relative to @ref m_anchor.
		 * @param [in] size The clickable size of the current element.
		 * @param [in] text The text of this element. Not every element needs a @ref m_text member. This is optional.
		 * @note @ref m_size does not always correspond to the perceivable size!
		 */
		Groupbox(
			BaseElement* parent,
			const Vector2D& position,
			const Vector2D& size,
			const std::string& text
		)
			: BaseElement( parent, position, size, text, kType_Groupbox ) {
			// Get width of the current @ref m_text string in pixels
			auto h = 0;
			g_renderer->GetTextSize( m_text.c_str(), m_text_width, h );
		}

		/**
		 * @brief Invoked when the user clicks somewhere on the element's surface.
		 * @return true, if the input was consumed.
		 * @return false, if the input wasn't consumed.
		 */
		bool OnPress() override {
			for( const auto& child : m_children ) {
				// Handle mouse events specifically based on element type
				// i.e. listbox has a scroll event which can turn focus on
				auto mouse_event = g_mouse->Pressing();
				if( child->m_type == kType_Listbox )
					mouse_event |= ( g_mouse->m_scroll_direction != kScroll_None );

				if( !mouse_event )
					continue;

				// There is currently no focused element
				// and the current element isn't being pressed on,
				// so we can just skip any focus callback logic
				if( !g_focused_element && !child->Hovered() )
					continue;

				// Handle the @ref OnPress callback of the newly focused element only once
				if( !g_focused_element && child->OnPress() ) {
					g_focused_element = child.get();
					g_focused_element->m_state |= kState_Focused;
					return true;
				}
			}

			return false;
		}

		/**
		 * @brief Called after the @ref OnPress callback was invoked and the mouse click was released.
		 */
		void OnRelease() override {}

		/**
		 * @brief Adds elements to the rendering engine queue.
		 * @note This function renders the underlying child elements.
		 */
		void Render() override {
			const auto position = GetAbsolutePosition();

			// Draw top line
			g_renderer->Line(
				position.m_x,
				position.m_y,
				position.m_x + 10,
				position.m_y,
				argb( 40, 40, 40 )
			);

			// Draw the continuation of the top line after @ref m_text
			g_renderer->Line(
				position.m_x + 15 + m_text_width + 5,
				position.m_y,
				position.m_x + m_size.m_x - 1,
				position.m_y,
				argb( 40, 40, 40 )
			);

			// Draw @ref m_text
			g_renderer->Text(
				position.m_x + 15,
				position.m_y,
				argb( colors::White ),
				kAlign_Left | kAlign_CenterV,
				"%s",
				m_text.c_str()
			);

			// Draw bottom line
			g_renderer->Line(
				position.m_x,
				position.m_y + m_size.m_y,
				position.m_x + m_size.m_x - 1,
				position.m_y + m_size.m_y,
				argb( 40, 40, 40 )
			);

			// Draw left line
			g_renderer->Line(
				position.m_x,
				position.m_y,
				position.m_x,
				position.m_y + m_size.m_y,
				argb( 40, 40, 40 )
			);

			// Draw right line
			g_renderer->Line(
				position.m_x + m_size.m_x - 1,
				position.m_y,
				position.m_x + m_size.m_x - 1,
				position.m_y + m_size.m_y,
				argb( 40, 40, 40 )
			);

			// Render underlying child elements in reverse order,
			// so they can be rendered properly
			for( const auto& child : ReverseRange( m_children ) )
				child->Render();
		}

		/**
		 * @brief Updates the @ref m_anchor member of the current element.
		 * @param [in] point The new @ref m_anchor point.
		 * @note This function moves the underlying child elements.
		 */
		void Move( const Vector2D& point ) override {
			// Update current element's anchor
			BaseElement::Move( point );

			// Move all elements alongside the anchor point
			for( auto& child : m_children )
				child->Move( m_position + point );
		}

		/**
		 * @brief Constructs and adds a new element to the @ref m_children container.
		 * @param [in] args Parameter pack for type T.
		 * @tparam T Type of the current element.
		 * @tparam Args Types of the parameter pack.
		 */
		template< typename T, typename... Args >
		T* Add( Args&&... args ) {
			// Get appropriate element height for this type of element
			constexpr auto element_height = ElementGeometry< T >::kHeight;

			// Element width is equal to size of current groupbox
			// minus left and right margin
			const auto element_width = m_size.m_x - ( kMargin.m_x * 2 );

			// Create new element and add it to our element list
			auto* element = m_children.emplace_back( new T(
				this,
				{ kMargin.m_x, m_height },
				{ element_width, element_height },
				std::forward< Args >( args )...
			) ).get();

			// Accumulate the total height of all child elements
			m_height += element_height + ( kMargin.m_y / 2 );

			// Adjust groupbox size according to the height of it's children
			if( m_height >= m_size.m_y )
				m_size.m_y = m_height;

			return static_cast< T* >( element );
		}
	};
}

#endif // !LIBSUI_ELEMENTS_GROUPBOX_HPP
