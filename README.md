# SimpleUserInterface (SUI)

---

SimpleUserInterface (or SUI for short) is an easy-to-use, intuitive C++17 library for straightforward UIs.
The idea behind SUI is to provide a starting base for simple, straightforward UIs, which should consequently be developed on top of it.
This means that local modifications to the code-base are welcome (and, in fact, intended).

Unfortunately, due to it's simplistic nature, SUI doesn't offer any text-wrapping, nor view-clipping capabilities, but rather only the most basic features.

# Screenshots
![Screenshot 1](screenshots/1.png)
![Screenshot 2](screenshots/2.png)
![Screenshot 3](screenshots/3.png)

* Note - The pixel imperfections are solely due to SDL2 not playing along on my system. With other renderers this defect vanishes.
* Note - The I and F letters denote that the window is either interactable (I) or focused (F) or both.

# Documentation
https://fouriertransform.bitbucket.io/SimpleUserInterface

# Integration
Since this is a header-only library, you can simply move the `include/` folder to your project and include the primary `libsui.hpp` file.

You build install it on your system by doing:
```
mkdir build && cd build
cmake .. -DOPT_INSTALL_LIBRARY=1 -DOPT_BUILD_DOCUMENTATION=0
make
make install
```

Or you can clone the code-base in its entirety and utilize CMake:
```
add_subdirectory(libsui)

# add_library or add_executable
target_link_libraries(sui-dev-env libsui)
```

# Available elements
- [Window](include/libsui/elements/window.hpp)
- [Tab](include/libsui/elements/tab.hpp)
- [Panel with an automated layout positioning system](include/libsui/elements/auto_panel.hpp)
- [Panel with a manually-controlled layout system](include/libsui/elements/panel.hpp)
- [Groupbox](include/libsui/elements/groupbox.hpp)
- [Slider](include/libsui/elements/slider.hpp)
- [Button](include/libsui/elements/button.hpp)
- [Checkbox](include/libsui/elements/checkbox.hpp)
- [Dropdown](include/libsui/elements/dropdown.hpp)
- [Listbox](include/libsui/elements/listbox.hpp)
- [Scrollbar](include/libsui/elements/scrollbar.hpp)
- [Textbox (SDL2 specific impl.)](include/libsui/elements/textbox.hpp)
- [Messagebox](include/libsui/elements/messagebox.hpp)

# Basic example
```cpp
// ...
#include <libsui/libsui.hpp>

void SetupMenu() {
	// NOTE: It is expected of you to provide a custom rendering backend implementation in accordance with the libsui::IRenderer interface. Ditto for mouse handling.
	libsui::Setup( &renderer, &mouse );

	// Default-construct the menu
	// NOTE: A libsui::Menu instance should exist for the duration of the user interface, i.e. application.
	libsui::Menu menu{};

	// Construct a window and attach the window to the menu instance.
	// The window will:
	// - have an automatic layout positioning panel
	// - start at X = 100 and Y = 50
	// - be of size W = 620 and H = 270
	// - be captioned "Main window"
	auto main_window = menu.Add< libsui::AutoPanel >(
		/*position = */{ 100, 50 },
		/*size = */{ 620, 270 },
		/*name = */"Main window"
	);

	// Setup main window
	main_window->Setup(
		{ "First", "Second" },
		[]( libsui::Window< libsui::AutoPanel >::Container& tabs ) {
			// Setup first groupbox
			static bool checkbox_one = true;
			static bool checkbox_two = false;
	
			auto group_one = tabs[ 0 ]->Add( "Group One" );
			group_one->Add< Checkbox >( "Checkbox one", checkbox_one );
			group_one->Add< Checkbox >( "Checkbox two", checkbox_two );
	
			// Setup second groupbox
			static bool checkbox_three = true;
			static float slider_one = 0.f;
	
			auto group_two = tabs[ 1 ]->Add( "Group Two" );
			group_two->Add< Checkbox >( "Checkbox three", checkbox_three );
			group_two->Add< Slider< float > >( "Slider one", -5.f, 5.f, slider_one );
		}
	);

	// Apply focus to the main window
	main_window->m_state |= kState_Focused;

	// Construct a second window and attach it to the menu
	auto second_window = menu.Add< libsui::AutoPanel >(
		/*position = */{ 740, 50 },
		/*size = */{ 360, 270 },
		/*name = */"Second window"
	);

	// Setup second window
	second_window->Setup(
		{ "One", "Two", "Three" },
		[]( libsui::Window< libsui::AutoPanel >::Container& tabs ) {
			tabs[ 0 ]->Add( "First" )->Add< Textbox >( "Some text" );
			tabs[ 0 ]->Add( "Second" )->Add< Button >( "Do something" );
		}
	);

	// NOTE: This is just an example loop. This should essentially be called on every rendering frame.
	// See include/libsui/main.cpp for more info.
	while( !should_exit ) {
		// ...

		// Update mouse position and left-click events
		mouse.Update();

		// NOTE: Here you need to poll mouse scroll events for listboxes to work properly.
		// See include/libsui/main.cpp for more info.

		// Handle menu input events and rendering
		menu.HandleInput();
		menu.Render();

		// Render our mouse
		mouse.Render();

		// ...
		// And here you should reset the mouse scroll events for the next frame
	}
}
```

For a more complex see the main.cpp file of the development environment included in the repository.

# Development environment
In essence, this is only used for fast prototyping. It isn't supposed to be "clean" nor optimized.

To get it up and running, both SDL2 and SDL2 TTF libraries are required. You will also have to change the `kFontPath` constant variable in [sdl2_renderer.hpp](include/libsui/sdl2_renderer.hpp) to point to a valid TTF font path on your system.
